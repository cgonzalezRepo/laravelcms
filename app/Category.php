<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	protected $fillable = [
        'name', 'description', 'user_id', 'slug'
    ];

	public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function post()
    {
        return $this->belongsToMany(Post::class, 'post_categories')->withTimestamps();
    }
}
