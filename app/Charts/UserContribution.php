<?php

namespace App\Charts;
use App\Post;
use App\Category;

use ConsoleTVs\Charts\Classes\Chartjs\Chart;

class UserContribution extends Chart
{
    /**
     * Initializes the chart.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function dataSetPostsByUser($userId)
    {
    	$totalPosts = Post::all()->count();
    	$postsByUser = Post::where('user_id', $userId)->count();
        $others = $totalPosts - $postsByUser; 

    	return array($others, $postsByUser);
    }

    public function dataSetCategoryByUser($userId)
    {
    	$totalCategories = Category::all()->count();
    	$categoriesByUser = Category::where('user_id', $userId)->count();
        $others = $totalCategories - $categoriesByUser;

    	return array($others, $categoriesByUser);
    }
}
