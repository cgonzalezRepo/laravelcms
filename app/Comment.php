<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
	 /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'post_id', 'body', 'name', 'email', 'website', 'user_ip', 'author'
    ];

    protected $hidden = [
        'user_ip', 'author'
    ];

	public function post()
    {
        return $this->belongsTo('App\Post');
    }
}