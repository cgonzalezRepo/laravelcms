<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CategoryStoreRequest;
use App\Http\Requests\CategoryUpdateRequest;
use App\Category;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('checkRole:1|2');
    }

    public function index()
	{
    	$categories = Category::all();
    	return view('manager.categories.all', ['categories' => $categories]);
	}
    
    public function show($id)
    {
        $category = Category::findOrFail($id);
        return view('manager.categories.show', compact('category')); 
    }

	public function create()
	{
    	return view('manager.categories.create');
	}

	public function store(CategoryStoreRequest $request)
	{
	    $validated = $request->validated();
    	auth()->user()->categories()->create($validated);
    	return redirect()->route('categorias.index');
	}

	public function edit($id)
    {
        $category = Category::findOrFail($id);
        return view('manager.categories.edit', compact('category'));
    }

    public function update(CategoryStoreRequest $request, $id) {
        $validated = $request->validated();
        $category = Category::findOrFail($id);
        $category->update($validated);;
        return redirect()->route('categorias.index');
    }

	public function destroy($id)
    {
    	Category::findOrFail($id)->delete();
    	return redirect()->route('categorias.index');
    }

    public function quit($categoryId, $postId)
    {
        $category = Category::findOrFail($categoryId);
        $category->post()->detach($postId);
        return redirect()->route('categorias.show', ['id' => $categoryId]);
    }
}
