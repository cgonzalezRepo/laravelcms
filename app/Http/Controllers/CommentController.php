<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CommentStoreRequest;
use App\Comment;
use App\Traits\GetAuthorCommentTrait;

class CommentController extends Controller
{
	use GetAuthorCommentTrait;

    public function store(CommentStoreRequest $request)
	{
        $validated = $request->validated();
        $validated['user_ip'] = $request->ip();
      	$validated['author'] = $this->getAuthorComment($validated['post_id']);

        $comment = new Comment;
    	$comment->create($validated);
    	return redirect()->back();
	}
}
