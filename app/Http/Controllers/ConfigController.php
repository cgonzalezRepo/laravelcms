<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Options;

class ConfigController extends Controller
{
    public function header()
    {
    	$options = Options::all()->keyBy('name');
        return view('manager.configuration.header', ['options' => $options]); 
    }
}