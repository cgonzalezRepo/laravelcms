<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{Post, Category, User};

class HomeController extends Controller
{
    public function index()
    {
        $posts = Post::latest()->where('visible', 1)->paginate(8);
        $favoritePosts = Post::all()->where('favorite', 1)->where('visible', 1);
        return view('pages.home', compact('posts', 'favoritePosts')); 
    }

    public function showPostByCategory($categorySlug)
    {
        $category = Category::where('slug', $categorySlug)->first();
        $categoryPosts = $category->post()->where('visible', 1)->paginate(8);
        return view('pages.category', compact('category', 'categoryPosts')); 
    }

	public function showPostByUser($username)
    {
        $user = User::where('username', $username)->first();
        $userPosts = $user->posts()->where('visible', 1)->paginate(8);
        return view('pages.autor', compact('user', 'userPosts')); 
    }

	public function showPostBySlug($slug)
    {
        $post = Post::where('slug', $slug)->first();
        $relatedPosts = Post::all()->where('visible', 1);
        return view('pages.post', compact('post', 'relatedPosts'));
    }

    public function search(Request $request) 
    {
        $search = $request->get("q");
        $posts = [];

        if (strlen($search) > 0) {
            $posts = Post::where('title', 'like', '%'. $search.'%')->orWhere('body', 'like', '%'. $search.'%')->orWhere('slug', 'like', '%'. $search.'%')->paginate(8);
            $posts->appends(['q' => $search]);
        }
        
        return view('pages.search', ['posts' => $posts, 'inputSearch' =>  $search]);
    }
}