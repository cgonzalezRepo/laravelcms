<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\{PostStoreRequest, PostUpdateRequest, PostUpdateFavoriteRequest};
use App\Post;
use App\Traits\ListCategoriesTrait;
use File;

class PostController extends Controller
{
    use ListCategoriesTrait;

    public function index()
	{
    	$posts = Post::latest()->get();
    	return view('manager.posts.all', ['posts' => $posts, 'articleCollapse' => 'articleAll']);
	}

    public function show($id)
    {
        $post = Post::findOrFail($id);
        return view('manager.posts.show', ['post' => $post, 'articleCollapse' => 'true']); 
    }

    public function showFavorites()
    {
        $posts = Post::all()->where('favorite', 1);
        return view('manager.posts.allFavorites', ['posts' => $posts, 'articleCollapse' => 'articleFavoriteAll']);
    }

	public function create()
	{
        $categories = $this->getAllCategories();
    	return view('manager.posts.create', compact('categories')); 
	}

	public function store(PostStoreRequest $request)
	{
        $validated = $request->validated();

        //El colaborador no puede publicar sus articulos
        if (auth()->user()->role_id == 4) {
            $validated['visible'] = 0;
            $validated['favorite'] = 0;
        }

        if($request->hasFile('postImage')) {
            $path = $request->file('postImage')->store('public/images');
            $validated['postImage'] = str_replace('public/images/', '', $path);
        }

    	$post = auth()->user()->posts()->create($validated);
        $post->categories()->attach($request->category);
    	return redirect()->route('articulos.index');
	}

	public function edit($id)
    {
        $categories = $this->getAllCategories();
        $post = Post::findOrFail($id);

        //El colaborador no puede editar articulos que no sean suyos
        //El autor no puede editar articulos que no sean suyos
        if (auth()->user()->role_id == 4 && $post->user->id != auth()->user()->id || auth()->user()->role_id == 3 && $post->user->id != auth()->user()->id) {
            return redirect()->route('articulos.index');
        }

        return view('manager.posts.edit',  ['post' => $post, 'categories' => $categories, 'articleCollapse' => 'true']); 
    }

	public function update(PostUpdateRequest $request, $id)
    {
        $validated = $request->validated();
        $post = Post::findOrFail($id);

        //El colaborador no puede editar articulos que no sean suyos 
        if (auth()->user()->role_id == 4 && $post->user->id != auth()->user()->id || auth()->user()->role_id == 3 && $post->user->id != auth()->user()->id) {
            return redirect()->route('articulos.index');
        }

        //El colaborador no puede publicar sus articulos
        if (auth()->user()->role_id == 4) {
            $validated['visible'] = 0;
            $validated['favorite'] = 0;
        }

        if($request->hasFile('postImage')) {
            $path = $request->file('postImage')->store('public/images');
            $validated['postImage'] = str_replace('public/images/', '', $path);
        }

        $post->update($validated);
        $post->categories()->sync($request->category); 
        return redirect()->route('articulos.index');
    }

	public function destroy($id)
    {
        $post = Post::findOrFail($id);

        //El colaborador no puede borrar articulos
        //El autor solo puede borrar sus articulos
        if (auth()->user()->role_id == 4 || auth()->user()->role_id == 3 && $post->user->id != auth()->user()->id) {
            return redirect()->route('articulos.index');
        }

        $post->delete();
    	return redirect()->route('articulos.index');
    }

    public function quit(PostUpdateFavoriteRequest $request, $id)
    {
        $validated = $request->validated();
        $post = Post::findOrFail($id);

        //El colaborador no puede quitar articulos favoritos
        if (auth()->user()->role_id == 4) {
            return redirect()->route('articulos.index');
        }

        $post->update($validated);
        return redirect()->route('articulos.showFavorites');
    }
}
