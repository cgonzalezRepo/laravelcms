<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('checkRole:1');
    }

    public function index()
	{
    	$roles = Role::all();
    	return view('manager.roles.all', ['roles' => $roles]);
	}

    public function show($id)
    {
        $role = Role::findOrFail($id);
        return view('manager.roles.show', compact('role')); 
    }
}