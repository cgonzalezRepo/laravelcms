<?php

namespace App\Http\Controllers;

use App\Http\Requests\SocialStoreRequest;
use Illuminate\Http\Request;
use App\Social;

class SocialController extends Controller
{
	public function __construct()
    {
        $this->middleware('checkRole:1');
    }

    public function index()
	{
    	$socials = Social::all();
    	return view('manager.socials.all', ['socials' => $socials]);
	}

    public function show($id)
    {
        $social = Social::findOrFail($id);
        return view('manager.socials.show', compact('social')); 
    }

	public function create()
	{
    	return view('manager.socials.create');
	}

	public function store(SocialStoreRequest $request)
	{
	    $validated = $request->validated();
	    $validated['icon'] = $validated['icon'];
    	Social::create($validated);
    	return redirect()->route('redes.index');
	}

	public function edit($id)
    {
        $social = Social::findOrFail($id);
        return view('manager.socials.edit', compact('social'));
    }

    public function update(SocialStoreRequest $request, $id) {
        $validated = $request->validated();
        $social = Social::findOrFail($id);
        $social->update($validated);;
        return redirect()->route('redes.index');
    }

	public function destroy($id)
    {
    	Social::findOrFail($id)->delete();
    	return redirect()->route('redes.index');
    }
}
