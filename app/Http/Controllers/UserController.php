<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\UserUpdateRequest;
use App\Http\Requests\UserStoreRequest;
use App\Traits\ListRolesTrait;
use Illuminate\Support\Facades\Hash;
use App\Charts\UserContribution;

class UserController extends Controller
{
    use ListRolesTrait;

	public function __construct()
    {
        $this->middleware('checkRole:1');
    }

    public function index()
	{
    	$users = User::all();
    	return view('manager.users.all', ['users' => $users]);
	}

    public function show($id)
    {
        $user = User::findOrFail($id);
        $chartPost = new UserContribution;
        $chartPost->labels(['Otros', 'Tú']);
        $chartPost->dataset("DataSetPost", "doughnut", $chartPost->dataSetPostsByUser($user->id))->options([
            'backgroundColor' => ['#cecdcd', '#1d81a2'],
            'hoverBackgroundColor' => ['#a6a6a6', '#18667f']
        ]);

        $chartCategory = new UserContribution;
        $chartCategory->labels(['Otros', 'Tú']);
        $chartCategory->dataset("DataSetCategory", "doughnut", $chartCategory->dataSetCategoryByUser($user->id))->options([
            'backgroundColor' => ['#cecdcd', '#1d81a2'],
            'hoverBackgroundColor' => ['#a6a6a6', '#18667f']
        ]);

        
        return view('manager.users.show', compact('user', 'chartPost', 'chartCategory')); 
    }

	public function create()
	{
        $roles = $this->getAllRoles();
        return view('manager.users.create', compact('roles')); 
	}

	public function store(UserStoreRequest $request)
	{
	    $validated = $request->validated();
        $validated["password"] = Hash::make($validated["password"]);

    	User::create($validated);
    	return redirect()->route('usuarios.index');
	}

    public function edit($id)
    {
        $roles = $this->getAllRoles();
        $user = User::findOrFail($id);
        return view('manager.users.edit', compact('user', 'roles'));
    }

	public function update(UserUpdateRequest $request, $id) {
        $validated = $request->validated();

        if (isset($validated["password"]))
            $validated["password"] = Hash::make($request->password);
       
        $user = User::findOrFail($id);
        $user->update($validated);
        return redirect()->route('usuarios.index');
    }

	public function destroy($id)
    {
    	User::findOrFail($id)->delete();
    	return redirect()->route('usuarios.index');
    }
}
