<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $userRoleId)
    {
        $user = $request->user();
        $roles = $request->user()->role;

        $checkRolesId = explode('|', $userRoleId);

        if (!in_array($user->role_id,$checkRolesId)) {
            return redirect('/manager/articulos');
        }
        
        return $next($request);
    }
}
