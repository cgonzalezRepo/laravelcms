<?php

namespace App\Http\Middleware;

use Closure;
use View;

use App\{Social, Category, Options};

class ShareData
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $categories = Category::has('post', '>' , 0)->with('post')->get();
        $socials = Social::all()->where('visible', 1);
        $options = Options::all()->keyBy('name');
        View::share(['categories' => $categories, 'socials' => $socials, 'options' => $options]);
        return $next($request);
    }
}
