<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       return [
            'title' => 'required',
            'body' => 'required',
            'slug' =>  'required|unique:posts',
            'metaTitle' => 'required',
            'metaDescription' => 'required',
            'postImage' => 'required|image|max:2048',
            'visible' => 'required|numeric',
            'favorite' => 'required|numeric',
        ];
    }

    public function messages()
    {
        return [
            'slug.unique' => 'La URL debe ser unica!',
            'metaTitle.required' => 'El meta título es un campo obligatorio!',
        ];
    }
}
