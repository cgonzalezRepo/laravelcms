<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PostUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $slug = $this->request->get("slug");
        return [
            'title' => 'required',
            'body' => 'required',
            'metaTitle' => 'required',
            'metaDescription' => 'required',
            'postImage' => 'image|max:2048',
            'slug' => ['required', Rule::unique('posts')->ignore($slug,'slug')],
            'visible' => 'required|numeric',
            'favorite' => 'required|numeric'
        ];
    }

    public function messages()
    {
        return [
            'slug.unique' => 'La URL debe ser unica!',
            'metaTitle.required' => 'El meta título es un campo obligatorio!',
        ];
    }
}
