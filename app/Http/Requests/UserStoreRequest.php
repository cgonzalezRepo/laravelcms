<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|max:25',
            'firstname' => 'nullable|max:25',
            'lastname' => 'nullable|max:50',
            'website' => 'max:255|nullable|url',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8|confirmed',
            'role_id' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'email.unique' => 'El email ya está en uso.',
            'email.required' => 'El email es un campo obligatorio.',
            'email.email' => 'El email es debe tener un formato válido.',
            'username.required' => 'El nombre es un campo obligatorio.',
            'firstname.max' => 'El apellido solo puede tener 25 caracteres máximo.',
            'username.max' => 'El nombre de usuario solo puede tener 25 caracteres máximo.',
            'lastname.max' => 'El nombre solo puede tener 25 caracteres máximo.',
            'website.max' => 'La url solo puede tener 255 caracteres máximo.',
            'website.url' => 'La url debe ser una url válida.',
            'role_id' => 'Debes definir un rol para el nuevo usuario.',
            'password.min' => 'La contraseña debe tener como mínimo 8 caracteres.',
            'password.confirmed' => 'La contraseña debe coincidir con la confirmación.',
            'password.required' => 'La contraseña es un campo obligatorio.'
        ];
    }
}
