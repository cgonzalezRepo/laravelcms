<?php

namespace App\Traits;

use Illuminate\Support\Facades\Auth;
use App\Post;

trait GetAuthorCommentTrait {
 
    public function getAuthorComment($postId) {

 		$result = false;

 		if(Auth::check()) {
 			$post = Post::findOrFail($postId);

 			if (Auth::user()->id == $post->user_id) {
 				$result = true;
 			}
        }else {
        	$result = false;
        }

        return $result;
    }
}