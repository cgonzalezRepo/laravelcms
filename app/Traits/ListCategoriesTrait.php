<?php

namespace App\Traits;

use App\Category;

trait ListCategoriesTrait {
 
    public function getAllCategories() {
 		$categories = Category::all();
 		return $categories;
    }
}