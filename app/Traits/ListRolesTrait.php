<?php

namespace App\Traits;

use App\Role;

trait ListRolesTrait {
 
    public function getAllRoles() {
 		$roles = Role::all();
 		return $roles;
    }
}