<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Category;
use Faker\Generator as Faker;

$factory->define(Category::class, function (Faker $faker) {
    return [
    	'name' => $faker->word,
        'description' => $faker->sentence,
        'user_id' =>  App\User::all(['id'])->random(),
        'slug' => $faker->slug, 
    	'metaTitle' => $faker->sentence,
    	'metaDescription' => $faker->sentence
    ];
});
