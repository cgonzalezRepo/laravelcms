<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Comment;
use Faker\Generator as Faker;

$factory->define(Comment::class, function (Faker $faker) {
    return [ 
        'post_id' => App\Post::all(['id'])->random(), 
        'body' => $faker->paragraph(10), 
        'name' => $faker->name, 
        'email' => $faker->email, 
        'website' => $faker->url, 
        'user_ip' => $faker->ipv4,
        'author' => false
    ];
});
