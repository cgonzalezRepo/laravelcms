<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\PostCategory;
use Faker\Generator as Faker;

$factory->define(PostCategory::class, function (Faker $faker) {
    return [
        'category_id' => App\Category::all(['id'])->random(),
        'post_id' => App\Post::all(['id'])->random()
    ];
});
