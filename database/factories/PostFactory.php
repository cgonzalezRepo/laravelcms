<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Post;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {
    return [
    	'title' => $faker->sentence,
    	'body' => $faker->text(600),
    	'slug' => $faker->slug, 
    	'metaTitle' => $faker->sentence,
    	'metaDescription' => $faker->sentence,
    	'user_id' => App\User::all(['id'])->random(),
    	'postImage' => $faker->image(storage_path('app/public/images'), 640,480, null, false),
    	'visible' => $faker->boolean(85),
    	'favorite' => $faker->boolean
    ];
});
