<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAutorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('autor', function (Blueprint $table) {
        //     $table->bigIncrements('id');
        //     $table->string('image');
        //     $table->string('imageDescription');
        //     $table->boolean('visible')->default(false);
        //     $table->string('title');
        //     $table->text('body');
        //     $table->string('metaTitle');
        //     $table->text('metaDescription');
        //     $table->timestamps();
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('autor');
    }
}
