<?php

use Illuminate\Database\Seeder;
use App\Category;
use Carbon\Carbon;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'user_id' => 1,
            'name' => 'Otros',
            'description' => 'Una categoría general para hablar de todo un poco.',
            'slug' => 'otros', 
            'metaTitle' => 'Categoría otros',
            'metaDescription' => 'categoría para hablar de todo un poco',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

       $count = 6;
       factory(Category::class, $count)->create();
    }
}