<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       $this->call([
            OptionsTableSeeder::class,
            SocialTableSeeder::class,
       		RolesTableSeeder::class,
	        UsersTableSeeder::class,
            CategoryTableSeeder::class,
            PostTableSeeder::class,
            PostCategoryTableSeeder::class,
            CommentTableSeeder::class
    	]);
    }
}
