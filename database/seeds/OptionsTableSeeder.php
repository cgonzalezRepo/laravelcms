<?php

use Illuminate\Database\Seeder;

class OptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('options')->insert([
            [
                'name' => 'header_image',
                'value' => 'images/logo.png',
            ],
            [
                'name' => 'header_image_alt',
                'value' => 'Logo de la web',
            ],
            [
                'name' => 'footer_text',
                'value' => 'Aenean vulputate nisl arcu, non consequat risus vulputate sed. Nulla eu sapien condimentum nisi aliquet sodales non et diam. Duis blandit nunc semper rutrum congue. Phasellus sed lacus ut odio vehicula varius. Etiam iaculis feugiat tortor ac ornare.',
            ],
            [
                'name' => 'footer_webname',
                'value' => 'Webname',
            ],
            [
                'name' => 'meet_autor_image',
                'value' => 'images/Upd_1.jpg',
            ],
            [
                'name' => 'meet_autor_image_alt',
                'value' => 'Imagen del autor',
            ],
            [
                'name' => 'meet_autor_text',
                'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ullamcorper eros sit amet aliquet dignissim. Donec nec libero finibus, vestibulum lacus id, blandit elit. Nunc non tincidunt ante. Curabitur accumsan pellentesque congue. Phasellus lectus nulla, posuere non augue nec, bibendum ultrices sem.',
            ]
        ]);
    }
}
