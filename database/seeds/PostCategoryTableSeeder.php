<?php

use Illuminate\Database\Seeder;
use App\PostCategory;

class PostCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $count = 30;
       factory(PostCategory::class, $count)->create();
    }
}
