<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
                'name' => 'Administrador',
                'description' => 'Todos los permisos',
            ],
            [
                'name' => 'Editor',
                'description' => 'Puede comentar, escribir, publicar, editar, eliminar, moderar sus artículos y los del resto. Y además administrar las categorías.',
            ],
            [
                'name' => 'Autor',
                'description' => 'Puede comentar, escribir, publicar, editar y eliminar sus propios artículos.',
            ],
            [
                'name' => 'Colaborador',
                'description' => 'Puede comentar y escribir sus artículos pero no publicarlos.',
            ],
            [
                'name' => 'Suscriptor',
                'description' => 'Sólo puede comentar.',
            ],
        ]);
    }
}