<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class SocialTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('socials')->insert([
            [
                'icon' => 'fab fa-instagram',
                'url' => 'https://www.instragram.com',
                'visible' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'icon' => 'fab fa-facebook-f',
                'url' => 'https://www.facebook.com',
                'visible' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'icon' => 'fab fa-pinterest-p',
                'url' => 'https://www.pinterest.com',
                'visible' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'icon' => 'fab fa-linkedin-in',
                'url' => 'https://www.linkedin.com',
                'visible' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'icon' => 'fab fa-twitter',
                'url' => 'https://www.twitter.com',
                'visible' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]
        ]);
    }
}
