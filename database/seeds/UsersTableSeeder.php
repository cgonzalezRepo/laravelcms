<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		    DB::table('users')->insert([
            'username' => 'Administrador',
            'firstname' => 'Administrador',
            'lastname' => 'Admin',
            'website' => 'https://127.0.0.1',
            'role_id' => 1,
            'email' => 'admin@admin.com',
            'password' => bcrypt('password'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $count = 5;
        factory(User::class, $count)->create();
    }
}