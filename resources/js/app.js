/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
// window.Vue = require('vue');

require('summernote');
require('datatables.net-bs4');
require('datatables.net-responsive-bs4');
require('summernote/dist/lang/summernote-es-ES.js');
require('bootstrap-select');
require('chart.js');
require('owl.carousel');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// const app = new Vue({
//     el: '#app',
// });

//Custom JS Home
$(document).ready(function() {
    //Carousel   
    $('#owl-carousel-post').owlCarousel({
        autoplay: true,
        nav:true,
        dots:false,
        navText: ['<i class="far fa-arrow-alt-circle-left"></i>','<i class="far fa-arrow-alt-circle-right"></i>'],
        navContainer: '#customNav',
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:3
            }
        }
    })

    $('#owl-carousel-home').owlCarousel({
        autoplay: true,
        nav:true,
        dots:false,
        navText: ['<i class="far fa-arrow-alt-circle-left"></i>','<i class="far fa-arrow-alt-circle-right"></i>'],
        navContainer: '#customNav',
        responsive:{
            0:{
                items:1
            },
            600:{
                items:4
            },
            1000:{
                items:4
            }
        }
    })
});


//Custom JS Backend
$(document).ready(function() {
	//revisar-conflicto con selectpicker
	// $('.dropdown').hover(function() {
	// 	$('.dropdown-menu').addClass('animated pulse');    
	// }, function() {
	//   	$('.dropdown-menu').removeClass("animated pulse");
	// });
 	$('#dataTable').DataTable({
    	dom: "<'row'<'col-sm-12 col-md-6'i><'col-sm-12 col-md-6'f>>" +
			"<'row'<'col-sm-12'tr>>" +
			"<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'<'float-right'p>>>"
    });


	//Estilizar tabla autocreada
	$('#dataTable_wrapper').removeClass('form-inline');

	$("#menu-toggle").click(function(e) {
		e.preventDefault();
		$("#wrapper").toggleClass("toggled");
    });

    $('#summernote').summernote({
    	lang: 'es-ES',
    	placeholder: 'Cuerpo del artículo',
    	tabsize: 2,
    	height: 500
	});


    if ($('#summernote').length) {
        //SummernoteFix
        $('.note-popover').css('display','none');
    }
   

  // <-------- Modal borrar, pasar ID --------->    
    var defaultDeleteForm = $('#deleteForm').attr('action');

  	$('#deleteModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
    		var objectId = button.data('id'); 
   	 	  $('#deleteForm').attr("action", defaultDeleteForm.replace('placeHolderId', objectId));
  	});

    //Al cerrar el modal dejamos el form como estaba
    $('#deleteModal').on('hidden.bs.modal', function () {
      $('#deleteForm').attr("action", defaultDeleteForm);
    });
  // <-------- End modal borrar, pasar ID --------->    

 
  // <-------- Modal quitar categoria, pasar ID --------->
    var defaultRemoveCategoryForm = $('#quitForm').attr('action');

    //Cambiamos el id al mostrar el modal
    $('#quitModal').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget);
    	var firstId = button.data('firstid'); 
    	var secondId = button.data('secondid'); 

  		$('#quitForm').attr("action", defaultRemoveCategoryForm.replace('firstHolderId', firstId));
  		var action = $('#quitForm').attr('action');
   	 	$('#quitForm').attr("action", action.replace('secondHolderId', secondId));
  	});

    //Al cerrar el modal dejamos el form como estaba
  	$('#quitModal').on('hidden.bs.modal', function () {
      $('#quitForm').attr("action", defaultRemoveCategoryForm);
  	});
  // <-------- END modal quitar categoria, pasar ID --------->


  // <-------- Modal quitar articulo favorito, pasar ID --------->
    var defaultRemoveFavoriteArticleForm = $('#removeFavoriteArticleForm').attr('action');
    
    //Cambiamos el id al mostrar el modal
    $('#removeFavoriteArticleModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var id = button.data('id'); 
        $('#removeFavoriteArticleForm').attr("action", defaultRemoveFavoriteArticleForm.replace('placeHolderId', id));
    });

    //Al cerrar el modal dejamos el form como estaba
    $('#removeFavoriteArticleModal').on('hidden.bs.modal', function () {
      $('#removeFavoriteArticleForm').attr("action", defaultRemoveFavoriteArticleForm);
    });
  // <-------- END modal quitar articulo favorito, pasar ID --------->


	//Boton ocultar/mostrar password
    $("#showHidePassword button").on('click', function(event) {
    	event.preventDefault();
        if($('#showHidePassword input').attr("type") == "text")	{
            $('#showHidePassword input').attr('type', 'password');
            $('#showHidePassword i').addClass( "fa-eye-slash" );
            $('#showHidePassword i').removeClass( "fa-eye" );
        }else if($('#showHidePassword input').attr("type") == "password") {
            $('#showHidePassword input').attr('type', 'text');
            $('#showHidePassword i').removeClass( "fa-eye-slash" );
            $('#showHidePassword i').addClass( "fa-eye" );
        }
    });

	//Boton ocultar/mostrar password repeat
    $("#showHidePasswordRepeat button").on('click', function(event) {
    	event.preventDefault();
        if($('#showHidePasswordRepeat input').attr("type") == "text")	{
            $('#showHidePasswordRepeat input').attr('type', 'password');
            $('#showHidePasswordRepeat i').addClass( "fa-eye-slash" );
            $('#showHidePasswordRepeat i').removeClass( "fa-eye" );
        }else if($('#showHidePasswordRepeat input').attr("type") == "password") {
            $('#showHidePasswordRepeat input').attr('type', 'text');
            $('#showHidePasswordRepeat i').removeClass( "fa-eye-slash" );
            $('#showHidePasswordRepeat i').addClass( "fa-eye" );
        }
    });
});  // document ready  

function resetForm(event){
    $('#quitForm').attr('action', event.data.param1);
}