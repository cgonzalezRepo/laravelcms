<div class="btn-group d-inline d-xl-none" role="group">
	<a href="{{route($show, $id)}}" role="button" class="btn btn-success"><i class="far fa-eye"></i></a>
 	<a href="{{route($edit, $id)}}" role="button" class="btn btn-primary"><i class="far fa-edit"></i></a>	
 	<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal" data-id="{{$id}}"><i class="far fa-trash-alt"></i></button>
</div>

<div class="d-none d-xl-block">
	<a href="{{route($show, $id)}}" role="button" class="btn btn-outline-success"><i class="far fa-eye"></i> @lang('Ver')</a>
		<a href="{{route($edit, $id)}}" role="button" class="btn btn-outline-primary"><i class="far fa-edit"></i> @lang('Editar')</a>
	<button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#deleteModal" data-id="{{$id}}"><i class="far fa-trash-alt"></i> @lang('Eliminar')</button>
</div>