<div class="row bg-full mb-5">
	<div class="col-12 mt-3">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a class="breadcrumb-link" href="{{route('home')}}">@lang('Inicio')</a></li>
				<li class="breadcrumb-item active" aria-current="page">{{$actualPageName}}</li>
			</ol>
		</nav>
	</div>
</div>