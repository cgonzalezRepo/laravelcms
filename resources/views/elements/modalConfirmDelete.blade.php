<div class="modal fade" id="{{$id}}Modal" tabindex="-1" role="dialog" aria-labelledby="deleteModalConfirmaton" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="row">
          <div class="col-3">
            <i class="fas fa-exclamation-triangle fa-4x text-danger"></i>
          </div>
          <div class="col-9">
            <div class="row">
              <div class="col-12">
                 <h5 class="font-weight-bold">{{$title}}</h5>
              </div>
            </div>
            <div class="row">
              <div class="col-12">
                <p>{{$message}}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <form id="{{$id}}Form" action="{{route($route, ['id' => 'placeHolderId'])}}" method="POST">
          @csrf
          @method('DELETE')
          <button type="button" class="btn" data-dismiss="modal">@lang('Cancelar')</button>
          <button class="btn btn-danger" type="submit">@lang('Eliminar')</button>
        </form>
      </div>
    </div>
  </div>
</div>