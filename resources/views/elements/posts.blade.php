@foreach ($posts as $post)
	<div class="col-12 {{$show}}">
		<a class="card custom-card" href="{{route('post', ['id' => $post->slug])}}">
			<div class="wrap">
				<img class="{{$images}}" src="{{ asset('storage/images/'.$post->postImage) }}" alt="Card image cap">
				<div class="middle">
					<button type="button" class="btn btn-lg btn-outline-light">@lang('Leer más')</button>
				</div>
			</div>
			<div class="card-custom-body">
				@foreach ($post->categories as $category)
					<span class="card-custom-title">{{$loop->first ? '' : ', '}}</span>
					<a href="{{route('category', ['id' => $category->slug])}}" class="card-custom-title no-decoration-link">{{$category->name}}</a>	
				@endforeach

				<a href="{{route('post', ['id' => $post->slug])}}" class="no-decoration-link">
					<h2 class="card-custom-subtitle gray-hover">{{$post->title}}</h2>
				</a>

				@if($body)
					<div class="card-custom-text-post">{{ str_limit(strip_tags($post->body), 200) }}</div>
				@endif
				
				<p class="card-custom-text">
					<a href="{{route('autor', ['id' => $post->user->username])}}" class="no-decoration-link gray-hover">{{$post->user->username}}</a>, <i class="far fa-clock"></i> {{ $post->created_at->diffForHumans() }}
					@if($comments)
						<a class="no-decoration-link gray-hover" href="{{route('post', ['id' => $post->slug]).'#comments'}}">
							<i class="far fa-comment ml-4"></i> {{$post->comments->count()}}
						</a>
					@endif
				</p>
			</div>
		</a>
	</div>

	@if($separator)
		<div class="col-12 d-block d-sm-none">
			<hr class="mb-5">
		</div>
	@endif
@endforeach