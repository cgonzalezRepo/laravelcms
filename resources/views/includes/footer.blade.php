<div class="container mt-5 py-5">
	<div class="row">
		<div class="col-12">
			<div class="separator-footer">
				<h3>@lang('Sobre nosotros')</h3>
			</div>
		</div>
	</div>

	<div class="row mb-3">
		<div class="col-12">
			<p class="footer-text">
				{{$options['footer_text']->value}}
			</p>
		</div>
	</div>

	<div class="row footer-links mb-5">
		<div class="col-12 col-md-4 mb-3 mb-md-0">
			<a href="#">@lang('Sitemap')</a>
		</div>

		<div class="col-12 col-md-4 mb-3 mb-md-0 text-md-center text-left">
			<a href="#">@lang('Aviso legal y política de privacidad')</a>
		</div>

		<div class="col-12 col-md-4 text-md-right text-left">
			
			<a href="#">@lang('Política de cookies')</a>
		</div>	
	</div>

	<div class="row">
		<div class="col-12 text-white">
			{{$options['footer_webname']->value}} © {{date("Y")}}
		</div>
	</div>

</div>

<script src="{{ asset('js/app.js') }}"></script>