<nav class="navbar navbar-expand-lg navbar-light mb-4 navbar-front">
  <div class="container">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item nav-item-front active">
          <a class="nav-link" href="{{route('home')}}">@lang('Inicio')</span></a>
        </li>
        <li class="nav-item nav-item-front dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            @lang('Blog')
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown"> 
            @foreach($categories as $category)
              <a class="dropdown-item" href="{{route('category', ['id' => $category->slug])}}">{{$category->name}}</a>
            @endforeach
          </div>
        </li>
        <li class="nav-item nav-item-front">
          <a class="nav-link" href="#">@lang('Contacto')</a>
        </li>
      </ul>
      <form class="form-inline my-2 my-lg-0" method="GET" action="{{ route('articulos.search') }}">
        <input class="form-control @error('q') is-invalid @enderror search-bar mr-sm-2 " name="q" type="search" placeholder="Buscar" aria-label="Buscar" value="{{ old('q') }}">
        <button class="btn btn-outline-dark my-2 my-sm-0" type="submit"><i class="fas fa-search"></i></button>
      </form>
    </div>
  </div>
</nav>