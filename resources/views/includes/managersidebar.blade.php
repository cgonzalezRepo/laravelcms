<!-- Sidebar -->
<div class="bg-light border-right" id="sidebar-wrapper">
  <div class="sidebar-heading">
    @lang('Admin Panel')
  </div>
  <ul class="list-group list-group-flush">
    <li class="{{Auth::user()->role_id != 1 ? "d-none" : "" }}">
      <a href="#collapseConfig" class="list-group-item list-group-item-action bg-light" data-toggle="collapse">
        <i class="fas fa-cog mr-1"></i> @lang('Configuración') <i class="fas fa-caret-down float-right"></i>
      </a>
    </li>
    {{-- Collapse from configuration --}}
    <ul class="list-group collapse" id="collapseConfig"> 
      <li>
        <a href="{{ route('configuracion.header') }}" class="list-group-item list-group-item-action bg-light-inside">
          <i class="far fa-circle ml-2 mr-1"></i> @lang('Cabecera')
        </a>
      </li>
    </ul>
    {{-- End collapse from configuration --}}
    <li class="{{Auth::user()->role_id != 1 ? "d-none" : "" }}">
      <a href="{{ route('usuarios.index') }}" class="list-group-item list-group-item-action bg-light">
        <i class="fas fa-users mr-1"></i> @lang('Usuarios')
      </a>
    </li>
    <li class="{{Auth::user()->role_id != 1 ? "d-none" : "" }}">
      <a href="{{ route('roles.index') }}" class="list-group-item list-group-item-action bg-light">
        <i class="fas fa-user-tag mr-1"></i> @lang('Roles')
      </a>
    </li>
    <li class="{{Auth::user()->role_id != 1 ? "d-none" : "" }}">
      <a href="{{ route('redes.index') }}" class="list-group-item list-group-item-action bg-light">
        <i class="fas fa-hashtag mr-1"></i> @lang('Redes sociales')
      </a>
    </li>
    <li>
      <a href="#collapseArticle" class="list-group-item list-group-item-action bg-light" data-toggle="collapse">
        <i class="far fa-newspaper mr-1"></i> @lang('Artículos') <i class="fas fa-caret-down float-right"></i>
      </a>
    </li>
    {{-- Collapse from posts --}}
    <ul class="list-group collapse {{isset($articleCollapse) ? "show" : "" }}" id="collapseArticle"> 
      <li>
        <a href="{{ route('articulos.index') }}" class="list-group-item list-group-item-action bg-light-inside {{isset($articleCollapse) && $articleCollapse == "articleAll" ? "active" : "" }}">
          <i class="far fa-circle ml-2 mr-1"></i> @lang('Ver todos')
        </a>
      </li>
      <li>
        <a href="{{ route('articulos.showFavorites') }}" class="list-group-item list-group-item-action bg-light-inside {{isset($articleCollapse) && $articleCollapse == "articleFavoriteAll" ? "active" : "" }}">
          <i class="far fa-circle ml-2 mr-1"></i> @lang('Ver favoritos')
        </a>
      </li>
    </ul>
    {{-- End collapse from posts --}}
    <li class="{{Auth::user()->role_id != 1 && Auth::user()->role_id != 2 ? "d-none" : "" }}">
      <a href="{{ route('categorias.index') }}" class="list-group-item list-group-item-action bg-light">
        <i class="fas fa-layer-group mr-1"></i> @lang('Categorías')
      </a>
    </li>
  </ul>
</div>
<!-- /#sidebar-wrapper -->