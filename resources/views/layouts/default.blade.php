<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @include('includes.head')
    </head>
    <body>
        <div class="container my-4">
            <div class="row">
                <div class="col-12 text-center">
                    <a href="{{route('home')}}">
                        <img class="logo" src="{{ asset($options['header_image']->value) }}" alt="{{$options['header_image_alt']->value}}">
                    </a>    
                </div>
            </div>
        </div>

        <header class="sticky-top">
            @include('includes.header')
        </header>
        <main>
            <div class="container"><!-- start container -->
                @yield('topContent')
                
                <div class="row"><!-- start 3 row -->
                    <div class="col-12 col-lg-9"><!-- start col-8 -->
                        @yield('content')  
                    </div><!-- end col-8 -->

                    <div class="col-12 col-lg-3"><!-- start col 4 -->
                        <div class="row">
                            <div class="col-12 col-md-6 col-lg-12 mb-4"><!-- MEET THE AUTHOR -->
                                <div class="row">
                                    <div class="col-12">
                                        <div class="separator"><h3>@lang('Conoce al autor')</h3></div>
                                    </div>              
                                    <div class="col-12 mb-3">
                                        <img class="img-sidebar" src="{{ asset($options['meet_autor_image']->value) }}" alt="{{$options['meet_autor_image_alt']->value}}">
                                    </div>
                                
                                    <div class="col-12">
                                        <p class="description">{!!$options['meet_autor_text']->value!!}</p>
                                    </div>
                                </div>
                            </div><!-- END MEET THE AUTHOR -->
                           
                            <div class="col-12 col-md-6 col-lg-12"> 
                                @if(count($socials) > 0) <!-- SOCIAL MEDIA BLOCK -->
                                    <div class="row mb-5">
                                        <div class="col-12">
                                            <div class="separator"><h3>@lang('Redes sociales')</h3></div>
                                        </div>

                                        <div class="col-12">
                                            @foreach($socials as $social)
                                                <a target="_blank" rel="nofollow" href="{{$social->url}}" class="social-media gray-hover">
                                                    <i class="{{$social->icon}}"></i>
                                                </a>
                                            @endforeach
                                        </div>
                                    </div>
                                @endif <!-- END SOCIAL MEDIA BLOCK -->

                                <div class="row"><!-- CATEGORIES BLOCK -->
                                    <div class="col-12">
                                        <div class="separator"><h3>@lang('Categorías')</h3></div>

                                        <ul class="ul-category">
                                            @foreach($categories as $category)
                                                <li class="text-uppercase"> 
                                                    <a href="{{route('category', ['id' => $category->slug])}}" class="categories-link gray-hover">
                                                    {{$category->name}} <span class="float-right">{{count($category->post->where('visible',1))}}</span>
                                                    </a>
                                                    <div class="categories-separator my-2"></div>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div><!-- END CATEGORIES BLOCK -->
                            </div>
                        </div>      
                    </div><!-- end col 4 -->
                </div><!-- end 3 row -->
            </div><!-- end container -->
        </main>
        
        <footer class="footer">
            @include('includes.footer')
        </footer>
    </body>
</html>