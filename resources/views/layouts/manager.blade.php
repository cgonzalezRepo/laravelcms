<!DOCTYPE html>
<html lang="es">
    <head>
        @include('includes.head')
    </head>
    <body>
        <div class="d-flex" id="wrapper">
            @include('includes.managersidebar')
            
            <div id="page-content-wrapper">
                @include('includes.managerheader')

                <div class="container-fluid">
                    @yield('content')
                </div>
            </div>
        </div>
        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>