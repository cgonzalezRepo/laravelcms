<!DOCTYPE html>
<html>
    <head>
        @include('includes.head')
    </head>
    <body>
        <div class="container">

            <header class="row">
                @include('includes.header')
            </header>

            <main class="row">

                <!-- sidebar content -->
                <div class="col-md-4">
                    @include('includes.sidebar')
                </div>

                <!-- main content -->
                <div class="col-md-8">
                    @yield('content')
                </div>

            </main>

            <footer class="row">
                @include('includes.footer')
            </footer>

        </div>
    </body>
</html>