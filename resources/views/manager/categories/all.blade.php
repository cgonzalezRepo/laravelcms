@extends('layouts.manager')
@section('title', 'Listado de categorias')
@section('description', 'Listado de categorias')
@section('content')
	@include('elements.modalConfirmDelete', ['id' => 'delete', 'title' => 'Eliminar categoría', 'message' => 'Esta acción no se puede deshacer', 'route' => 'categorias.destroy'])

	<div class="row">
		<div class="col-12">
			<div class="backend-card">
	            <div class="card-header">
	            	<h1><i class="far fa-folder-open"></i> Categorias</h1>
	            </div>
	            <div class="card-body">
	                <div class="row">
	                    <div class="col-12">
							<a role="button" href="{{ route('categorias.create') }}" class="btn btn-success pull-left"><i class="fas fa-plus"></i> Nuevo</a>
	                    </div>
	                </div>
	                <div class="row">
	                    <div class="col-12">
							<table id="dataTable" class="table table-hover table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
								<thead class="thead-dark"> 
									<tr> 
										<th>ID</th>
										<th>Nombre</th>
										<th>Artículos</th>
										<th>Autor</th> 
										<th>Creado</th> 
										<th>Actualizado</th>
										<th>Acciones</th>
									</tr>
								</thead> 
								<tbody> 
									@foreach ($categories as $category)
										<tr> 
											<td>{{$category->id}}</td>
											<td>{{$category->name}}</td>
											<td>{{count($category->post)}}</td>
											<td><a href="{{route('usuarios.show', $category->user->id)}}">{{$category->user->username}}</a></td>
											<td>{{$category->created_at}}</td>
											<td>{{$category->updated_at}}</td>
											<td>
												<div class="btn-group d-inline d-xl-none" role="group">
													<a href="{{route('categorias.show', $category->id)}}" role="button" class="btn btn-success"><i class="far fa-eye"></i></a>
												 	<a href="{{route('categorias.edit', $category->id)}}" role="button" class="btn btn-primary"><i class="far fa-edit"></i></a>	
												 	<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal" data-id="{{$category->id}}"><i class="far fa-trash-alt"></i></button>
												</div>

												<div class="d-none d-xl-block">
													<a href="{{route('categorias.show', $category->id)}}" role="button" class="btn btn-outline-success"><i class="far fa-eye"></i> Ver</a>
				      								<a href="{{route('categorias.edit', $category->id)}}" role="button" class="btn btn-outline-primary"><i class="far fa-edit"></i> Editar</a>
													<button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#deleteModal" data-id="{{$category->id}}"><i class="far fa-trash-alt"></i> Eliminar</button>
												</div>
											</td>
										</tr>
									@endforeach
								</tbody> 
							</table>
	                    </div>
	                </div>
	            </div>
	        </div>
    	</div>
	</div>
@endsection

