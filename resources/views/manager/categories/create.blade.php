@extends('layouts.manager')
@section('title', 'Crear nueva categoria')
@section('description', 'Creación de nuevas categorias desde el backend')

@section('content')
    <form class="form-horizontal col-12" action="{{ route('categorias.store') }}" method="POST">
        @csrf
        <div class="row mt-3">
            <div class=" col-12 text-right">
                <a href="{{route('categorias.index')}}" role="button" class="btn btn-success"><i class="fas fa-arrow-left"></i> Volver</a>
                <button type="submit" class="btn btn-primary">Crear</button>
            </div>
        </div>

        <div class="row flex-column-reverse flex-xl-row">
            <div class="col-xl-8 col-12">
                <div class="backend-card">
                    <div class="card-header">
                        <div class="backend-card-title">Crear nueva categoría</div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-12">
                                <input type="text" class="form-control form-control-sm  @error('name') is-invalid @enderror" name="name" placeholder="Nombre" value="{{ old('name') }}">
                                @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>

                            <div class="form-group col-12">
                                <textarea class="form-control @error('description') is-invalid @enderror" name="description" placeholder="Descripción" rows="2">{{ old('description') }}</textarea>
                                @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                 @enderror
                            </div> 
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-4 col-12">       
                <div class="backend-card">
                    <div class="card-header">
                        <div class="backend-card-title">Apartado SEO</div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-12">                       
                                <input type="text" class="form-control form-control-sm  @error('metaTitle') is-invalid @enderror" name="metaTitle" placeholder="Meta título" value="{{ old('metaTitle') }}">
                                @error('metaTitle')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>

                            <div class="form-group col-12">
                                <input type="text" class="form-control form-control-sm @error('slug') is-invalid @enderror" name="slug" placeholder="URL" value="{{ old('slug') }}">
                                @error('slug')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                 @enderror
                            </div>

                            <div class="form-group col-12">
                                <textarea class="form-control @error('metaDescription') is-invalid @enderror" name="metaDescription" placeholder="Meta descripción" rows="2">{{ old('metaDescription') }}</textarea>
                                @error('metaDescription')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                 @enderror
                            </div>    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection