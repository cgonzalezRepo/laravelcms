@extends('layouts.manager')
@section('title', 'Editar categoría')
@section('description', 'Edición de una categoría desde el backend')

@section('content')
    <form class="form-horizontal col-12" action="{{ route('categorias.update', $category->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="row mt-3">
            <div class="col-12 text-right">
                <a href="{{ url()->previous() }}" role="button" class="btn btn-success"><i class="fas fa-arrow-left"></i> @lang('Volver')</a>
                <button type="submit" class="btn btn-primary">@lang('Actualizar')</button>
            </div>
        </div>

        <div class="row flex-column-reverse flex-xl-row">
            <div class="col-xl-8 col-12">
                <div class="backend-card">
                    <div class="card-header">
                        <div class="backend-card-title">Editar categoría</div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-12">
                                <label class="font-weight-bold">Nombre</label>
                                <input type="text" class="form-control form-control-sm  @error('name') is-invalid @enderror" name="name" placeholder="Nombre" value="{{ old('name', $category->name) }}">
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group col-12">
                                <label class="font-weight-bold">Descripción</label>
                                <textarea class="form-control @error('description') is-invalid @enderror" name="description" placeholder="Descripción" rows="2">{{ old('description', $category->description) }}</textarea>
                                @error('description')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                             @enderror
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-4 col-12">
                <div class="backend-card">
                    <div class="card-header">
                        <div class="backend-card-title">Apartado SEO</div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-12">
                                <label class="font-weight-bold">Meta título</label>
                                <input type="text" class="form-control form-control-sm  @error('metaTitle') is-invalid @enderror" name="metaTitle" placeholder="Meta título" value="{{ old('metaTitle', $category->metaTitle) }}">
                                @error('metaTitle')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>

                            <div class="form-group col-12">
                                <label class="font-weight-bold">Url</label>
                                <input type="text" class="form-control form-control-sm @error('slug') is-invalid @enderror" name="slug" placeholder="URL" value="{{ old('slug', $category->slug) }}">
                                @error('slug')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                 @enderror
                            </div>

                            <div class="form-group col-12">
                                <label class="font-weight-bold">Meta descripción</label>
                                <textarea class="form-control @error('metaDescription') is-invalid @enderror" name="metaDescription" placeholder="Meta descripción" rows="2">{{ old('metaDescription', $category->metaDescription) }}</textarea>
                                @error('metaDescription')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
    </form>
@endsection