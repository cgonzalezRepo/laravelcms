<div class="modal fade" id="quitModal" tabindex="-1" role="dialog" aria-labelledby="quitArticlefromCategoryModalConfirmaton" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="row">
          <div class="col-3">
            <i class="fas fa-exclamation-triangle fa-4x text-danger"></i>
          </div>
          <div class="col-9">
            <div class="row">
              <div class="col-12">
                 <h5 class="font-weight-bold">Quitar artículo de la categoría</h5>
              </div>
            </div>
            <div class="row">
              <div class="col-12">
                <p>Esta acción no se puede deshacer</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <form id="quitForm" action="{{route('categorias.quit', ['firstId' => 'firstHolderId', 'secondId' => 'secondHolderId'])}}" method="POST">
          @csrf
          <button type="button" id="cancel" class="btn" data-dismiss="modal">Cancelar</button>
          <button class="btn btn-danger" type="submit">Quitar</button>
        </form>
      </div>
    </div>
  </div>
</div>