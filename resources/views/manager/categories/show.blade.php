@extends('layouts.manager')
@section('title', 'Ver categoría')
@section('description', 'Ver una categoría desde el backend')

@section('content')
@include('manager.categories.modalConfirmQuit')

    <div class="row mt-3">
        <div class=" col-12 text-right">
            <a href="{{route('categorias.index')}}" role="button" class="btn btn-success"><i class="fas fa-arrow-left"></i> Volver</a>
            <a href="{{route('categorias.edit', $category->id)}}" role="button" class="btn btn-primary"><i class="far fa-edit"></i> Editar</a> 
        </div>
    </div>

    <div class="row flex-column-reverse flex-xl-row">
        <div class="col-xl-6 col-12">
            <div class="backend-card">
                <div class="card-header">
                    <div class="backend-card-title">Vista previa de la categoría</div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <label class="font-weight-bold">Nombre: </label><p class="d-inline"> {{$category->name}}</p>
                        </div>

                        <div class="col-12">
                             <label class="font-weight-bold">Descripción: </label><p class="d-inline"> {{$category->description}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-6 col-12">
            <div class="backend-card">
                <div class="card-header">
                    <div class="backend-card-title">Apartado SEO</div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <label class="font-weight-bold">Link: </label><a href="{{$category->slug}}"> {{route('home')}}/{{$category->slug}}</a>
                        </div>

                        <div class="col-12">
                             <label class="font-weight-bold">Título de la página: </label><p class="d-inline"> {{$category->metaTitle}}</p>
                        </div>

                        <div class="col-12">
                            <label class="font-weight-bold">Descripción de la página: </label><p class="d-inline"> {{$category->metaDescription}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="backend-card">
                <div class="card-header">
                    <div class="backend-card-title">Artículos que contienen esta categoría</div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <table id="dataTable" class="table table-hover table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                <thead class="thead-dark"> 
                                    <tr> 
                                        <th>ID</th>
                                        <th class="datetable-title">Título</th>
                                        <th>Visible</th>
                                        <th>Creado</th>
                                        <th>Actualizado</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead> 
                                <tbody> 
                                    @foreach ($category->post as $post)
                                        <tr> 
                                            <td>{{$post->id}}</td>
                                            <td class="text-wrap">{{$post->title}}</td>
                                            <td class="text-center">
                                                @if($post->visible)
                                                    <i class="fas fa-check text-success"></i>
                                                @else
                                                    <i class="fas fa-times text-danger"></i>
                                                @endif
                                            </td>
                                            <td>{{$post->created_at}}</td>
                                            <td>{{$post->updated_at}}</td>
                                            <td>
                                                <div class="btn-group d-inline d-xl-none" role="group">
                                                    <a href="{{route('articulos.show', $post->id)}}" role="button" class="btn btn-success"><i class="far fa-eye"></i></a>
                                                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#quitModal" data-firstid="{{$category->id}}" data-secondid="{{$post->id}}"><i class="fas fa-minus-circle"></i></button>
                                                </div>

                                                <div class="d-none d-xl-block">
                                                    <a href="{{route('articulos.show', $post->id)}}" role="button" class="btn btn-outline-success"><i class="far fa-eye"></i> Ver</a>
                                                    <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#quitModal" data-firstid="{{$category->id}}" data-secondid="{{$post->id}}"><i class="fas fa-minus-circle"></i> Quitar</button>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody> 
                            </table>    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection