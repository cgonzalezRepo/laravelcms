@extends('layouts.manager')
@section('title', 'Configuración')
@section('description', '')
@section('content')
	
	<div class="row">
        <div class="col-6">    
            <div class="backend-card">
                <div class="card-header">
                    <div class="backend-card-title">@lang('Cabecera')</div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-12">
                            <label class="font-weight-bold">@lang('Imagen actual')</label>
                        </div>
                        <div class="form-group col-12">
                            <img class="img-thumbnail backend-image" src="{{ asset($options['header_image']->value) }}">
                        </div>
                        <div class="form-group col-12">
                            <input type="file" class="form-control-file @error('headerImage') is-invalid @enderror" name="headerImage">
                        
                            @error('headerImage')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-6">    
            <div class="backend-card">
                <div class="card-header">
                    <div class="backend-card-title">@lang('Footer')</div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-12">
                            <label class="font-weight-bold">@lang('Imagen actual')</label>
                        </div>
                        <div class="form-group col-12">
                            <img class="img-thumbnail backend-image" src="{{ asset($options['header_image']->value) }}">
                        </div>
                        <div class="form-group col-12">
                            <input type="file" class="form-control-file @error('headerImage') is-invalid @enderror" name="headerImage">
                        
                            @error('headerImage')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@extends('layouts.manager')
@section('title', 'Configuración')
@section('description', '')

@section('content')
    <form class="form-horizontal col-12" action="" method="POST">
        @csrf
        @method('PUT')
        <div class="row mt-3">
            <div class="col-12 text-right">
                <a href="{{ url()->previous() }}" role="button" class="btn btn-success"><i class="fas fa-arrow-left"></i> Volver</a>
                <button type="submit" class="btn btn-primary">Actualizar</button>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-6 col-12">
                <div class="backend-card">
                    <div class="card-header">
                        <div class="backend-card-title">Editar cabecera</div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-12">                            
                                <label class="font-weight-bold">Imagen actual</label>
                                <input type="text" class="form-control @error('username') is-invalid @enderror" name="username" placeholder="Nombre de usuario" value="{{ old('username', $user->username) }}">
                                @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                 @enderror
                            </div>
                            
                            <div class="form-group col-12">                            
                                <label class="font-weight-bold">Nombre de usuario</label>
                                <input type="text" class="form-control @error('username') is-invalid @enderror" name="username" placeholder="Nombre de usuario" value="{{ old('username', $user->username) }}">
                                @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                 @enderror
                            </div>

                            
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-6 col-12">
                <div class="backend-card">
                    <div class="card-header">
                        <div class="backend-card-title">Editar footer</div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-12">
                                <label class="font-weight-bold">Texto</label>
                                <input type="text" class="form-control @error('firstname') is-invalid @enderror" name="firstname" placeholder="Nombre" value="{{ old('firstname', $options['footer_text']->value) }}">
                                @error('firstname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                 @enderror
                            </div>

                            <div class="form-group col-12">
                                <label class="font-weight-bold">Nombre de la web</label>
                                <input type="text" class="form-control @error('lastname') is-invalid @enderror" name="lastname" placeholder="Apellidos" value="{{ old('lastname', $options['footer_webname']->value) }}">
                                @error('lastname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                 @enderror
                            </div>
                        </div>
                    </div>
                </div>    
            </div>
        </div>
    </form>
@endsection