@extends('layouts.manager')
@section('title', 'Listado de artículos')
@section('description', 'Listado de artículos')
@section('content')
	@include('elements.modalConfirmDelete', ['id' => 'delete', 'title' => 'Eliminar artículo', 'message' => 'Esta acción no se puede deshacer', 'route' => 'articulos.destroy'])

	<div class="row">
		<div class="col-12">
			<div class="backend-card">
	            <div class="card-header">
					<h1><i class="far fa-folder-open"></i> Artículos</h1>
	            </div>
	            <div class="card-body">
	                <div class="row">
	                    <div class="col-12">
							<a role="button" href="{{ route('articulos.create') }}" class="btn btn-success pull-left"><i class="fas fa-plus"></i> Nuevo</a>
	                    </div>
	                </div>
	                <div class="row">
	                    <div class="col-12">
							<table id="dataTable" class="table table-hover table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
								<thead class="thead-dark"> 
									<tr> 
										<th>Miniatura</th>
										<th class="datetable-title">Titulo</th>
										<th>Visible</th>
										<th>Autor</th> 
										<th>Categorias</th>
										<th>Creado</th> 
										<th>Actualizado</th>
										<th>Acciones</th>
									</tr>
								</thead> 
								<tbody> 
									@foreach ($posts as $post)
										<tr> 
											<td class="text-center"><img class="img-thumbnail backend-image-xs" src="{{ asset('storage/images/'.$post->postImage) }}"></td>
											<td class="text-wrap">{{ $post->title }}</td>
											<td class="text-center">
												<span class="d-none">{{$post->visible}}</span>
												@if($post->visible)
													<i class="fas fa-check text-success"></i>
												@else
													<i class="fas fa-times text-danger"></i>
												@endif
											</td>
											<td><a href="{{route('usuarios.show', $post->user->id)}}">{{$post->user->username}}</a></td>
											<td>
												@if($post->categories->count() > 0)
													@foreach ($post->categories as $category)
														{{$loop->first ? '' : ', '}}
														<a href="{{route('categorias.show', ['id' => $category->id])}}">{{$category->name}}</a> 	
													@endforeach
												@else
													Sin categoría
												@endif
												
											</td>
											<td>{{$post->created_at}}</td>
											<td>{{$post->updated_at}}</td>
											<td>
												<div class="btn-group d-inline d-xl-none" role="group">
													<a href="{{route('articulos.show', $post->id)}}" role="button" class="btn btn-success"><i class="far fa-eye"></i></a>
												 	
												 	@if($post->user->id == Auth::user()->id || Auth::user()->role_id == 1 || Auth::user()->role_id == 2)
												 		<a href="{{route('articulos.edit', $post->id)}}" role="button" class="btn btn-primary"><i class="far fa-edit"></i></a>
												 	@endif

												 	@if($post->user->id == Auth::user()->id && Auth::user()->role_id != 4 || Auth::user()->role_id == 1 || Auth::user()->role_id == 2)
												 		<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal" data-id="{{$post->id}}"><i class="far fa-trash-alt"></i></button>
												 	@endif
												</div>

												<div class="d-none d-xl-block">
													<a href="{{route('articulos.show', $post->id)}}" role="button" class="btn btn-outline-success"><i class="far fa-eye"></i> Ver</a>

													@if($post->user->id == Auth::user()->id || Auth::user()->role_id == 1 || Auth::user()->role_id == 2)
				      									<a href="{{route('articulos.edit', $post->id)}}" role="button" class="btn btn-outline-primary"><i class="far fa-edit"></i> Editar</a>
				      								@endif

				      								@if($post->user->id == Auth::user()->id && Auth::user()->role_id != 4 || Auth::user()->role_id == 1 || Auth::user()->role_id == 2)
				      									<button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#deleteModal" data-id="{{$post->id}}"><i class="far fa-trash-alt"></i> Eliminar</button>
				      								@endif
													
												</div>
											</td>
										</tr>
									@endforeach
								</tbody> 
							</table>
	                    </div>
	                </div>
	            </div>
	        </div>
    	</div>
	</div>
@endsection

