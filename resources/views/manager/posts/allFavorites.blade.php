@extends('layouts.manager')
@section('title', 'Listado de artículos favoritos')
@section('description', 'Listado de artículos favoritos')

@section('content')
    @include('manager.posts.modalConfirmFavoriteRemove')

    <div class="row">
        <div class="col-12">
            <div class="backend-card">
                <div class="card-header">
                    <h1><i class="far fa-folder-open"></i> Artículos favoritos</h1>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="alert alert-info" role="alert">
                                Los artículos de esta lista se mostrarán en la sección "Favoritos del autor" de la página inicial, eliminar todos los favoritos ocultará la sección.
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <table id="dataTable" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                <thead class="thead-dark"> 
                                    <tr> 
                                        <th>Miniatura</th>
                                        <th class="datetable-title">Titulo</th>
                                        <th>Visible</th>
                                        <th>Autor</th> 
                                        <th>Categorias</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead> 
                                <tbody> 
                                    @foreach ($posts as $post)
                                        <tr class="alert-success text-body"> 
                                            <td class="text-center"><img class="img-thumbnail backend-image-xs" src="{{ asset('storage/images/'.$post->postImage) }}"></td>
                                            <td class="text-wrap">{{$post->title}}</td>
                                            <td class="text-center">
                                                <span class="d-none">{{$post->visible}}</span>
                                                @if($post->visible)
                                                    <i class="fas fa-check text-success"></i>
                                                @else
                                                    <i class="fas fa-times text-danger"></i>
                                                @endif
                                            </td>
                                            <td><a href="{{route('usuarios.show', $post->user->id)}}">{{$post->user->username}}</a></td>
                                            <td>
                                                @if($post->categories->count() > 0)
                                                    @foreach ($post->categories as $category)
                                                        {{$loop->first ? '' : ', '}}
                                                        <a href="{{route('categorias.show', ['id' => $category->id])}}">{{$category->name}}</a>     
                                                    @endforeach
                                                @else
                                                    Sin categoría
                                                @endif
                                            </td>
                                            <td>
                                                <div class="btn-group d-inline d-xl-none" role="group">
                                                    <a href="{{route('articulos.show', $post->id)}}" role="button" class="btn btn-success"><i class="far fa-eye"></i></a>
                                                    
                                                    @if(Auth::user()->role_id == 1)
                                                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#removeFavoriteArticleModal" data-id="{{$post->id}}"><i class="fas fa-minus-circle"></i></button>
                                                    @endif

                                                </div>

                                                <div class="d-none d-xl-block">
                                                    <a href="{{route('articulos.show', $post->id)}}" role="button" class="btn btn-success"><i class="far fa-eye"></i> Ver</a>
                                                   
                                                    @if(Auth::user()->role_id == 1)
                                                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#removeFavoriteArticleModal" data-id="{{$post->id}}"><i class="fas fa-minus-circle"></i> Quitar</button>
                                                    @endif
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody> 
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

