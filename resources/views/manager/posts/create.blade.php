@extends('layouts.manager')
@section('title', 'Crear nuevo artículo')
@section('description', 'Creación de nuevos artículos desde el backend')

@section('content')
    <form class="form-horizontal col-12" action="{{ route('articulos.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row mt-3">
            <div class=" col-12 text-right">
                <a href="{{route('articulos.index')}}" role="button" class="btn btn-success"><i class="fas fa-arrow-left"></i> Volver</a>
                <button type="submit" class="btn btn-primary">Crear</button>
            </div>
        </div>

        <div class="row flex-column-reverse flex-xl-row">
            <div class="col-xl-8 col-12">
                <div class="backend-card">
                    <div class="card-header">
                        <div class="backend-card-title">Crear nuevo artículo</div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-12">
                                <input type="text" class="form-control @error('title') is-invalid @enderror" name="title" placeholder="Título" value="{{ old('title') }}">
                                @error('title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                 @enderror
                            </div>

                            <div class="form-group col-12">
                                <textarea id="summernote" class="form-control @error('body') is-invalid @enderror" name="body">{{ old('body') }}</textarea>
                                @error('body')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                 @enderror
                            </div>    
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-4 col-12">   
                <div class="row">
                    <div class="col-12">    
                        <div class="backend-card">
                            <div class="card-header">
                                <div class="backend-card-title">Apartado SEO</div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="form-group col-12">                       
                                        <input type="text" class="form-control form-control-sm  @error('metaTitle') is-invalid @enderror" name="metaTitle" placeholder="Meta título" value="{{ old('metaTitle') }}">
                                        @error('metaTitle')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                        @enderror
                                    </div>

                                    <div class="form-group col-12">
                                        <input type="text" class="form-control form-control-sm @error('slug') is-invalid @enderror" name="slug" placeholder="URL" value="{{ old('slug') }}">
                                        @error('slug')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                         @enderror
                                    </div>

                                    <div class="form-group col-12">
                                        <textarea class="form-control @error('metaDescription') is-invalid @enderror" name="metaDescription" placeholder="Meta descripción" rows="2">{{ old('metaDescription') }}</textarea>
                                        @error('metaDescription')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                         @enderror
                                    </div>    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">    
                        <div class="backend-card">
                            <div class="card-header">
                                <div class="backend-card-title">Miniatura</div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="form-group col-12">
                                        <div class="form-group">
                                            <input type="file" class="form-control-file @error('postImage') is-invalid @enderror" name="postImage">
                                        
                                            @error('postImage')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">    
                        <div class="backend-card">
                            <div class="card-header">
                                <div class="backend-card-title">Opciones</div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="form-group col-12">                            
                                        <div class="custom-control custom-switch">
                                            <input type="hidden" name="visible" value="0">
                                            <input type="checkbox" class="custom-control-input @error('visible') is-invalid @enderror" name="visible" id="visible" value="1" @if(old('visible')) checked @endif>
                                            <label class="custom-control-label" for="visible">Visible</label>
                                            @error('visible')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group col-12">                            
                                        <div class="custom-control custom-switch">
                                            <input type="hidden" name="favorite" value="0">
                                            <input type="checkbox" class="custom-control-input @error('favorite') is-invalid @enderror" name="favorite" id="favorite" value="1" @if(old('favorite')) checked @endif>
                                            <label class="custom-control-label" for="favorite">Favorito</label>
                                            @error('favorite')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">    
                        <div class="backend-card">
                            <div class="card-header">
                                <div class="backend-card-title">Categoría</div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="form-group col-12">
                                        <select class="form-control selectpicker" multiple name="category[]" title="Categoría">
                                            @foreach ($categories as $category)
                                                <option value="{{$category->id}}">{{$category->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection