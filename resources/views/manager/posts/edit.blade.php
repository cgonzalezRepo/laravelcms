@extends('layouts.manager')
@section('title', 'Editar artículo')
@section('description', 'Edición de un artículo desde el backend')

@section('content')
    <form class="form-horizontal col-12" action="{{ route('articulos.update', $post->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="row mt-3">
            <div class="col-12 text-right">
                <a href="{{ url()->previous() }}" role="button" class="btn btn-success"><i class="fas fa-arrow-left"></i> @lang('Volver')</a>
                <button type="submit" class="btn btn-primary">@lang('Actualizar')</button>
            </div>
        </div>
        <div class="row flex-column-reverse flex-xl-row">
            <div class="col-xl-8 col-12">
                <div class="backend-card">
                    <div class="card-header">
                        <div class="backend-card-title">@lang('Editar artículo')</div>
                    </div>
                    <div class="card-body">
                        <div class="row">                            
                            <div class="form-group col-12"> 
                                <label class="font-weight-bold">@lang('Título')</label>
                                <input type="text" class="form-control @error('title') is-invalid @enderror" name="title" placeholder="Título" value="{{ old('title', $post->title) }}">
                                @error('title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                 @enderror
                            </div>
                                
                            <div class="form-group col-12">
                                <label class="font-weight-bold">@lang('Categoría')</label>
                                <select class="form-control selectpicker" multiple name="category[]" title="Categoría">
                                    @foreach ($categories as $category)
                                        <option @if(in_array($category->id, $post->categories->pluck('id')->toArray())) selected @endif value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-12">
                                <label class="font-weight-bold">@lang('Cuerpo del artículo')</label>
                                <textarea id="summernote" class="form-control @error('body') is-invalid @enderror" name="body">{{ old('body', $post->body) }}</textarea>
                                @error('body')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                 @enderror
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-4 col-12">
                <div class="row">
                    <div class="col-12">
                        <div class="backend-card">
                            <div class="card-header">
                                <div class="backend-card-title">@lang('Apartado SEO')</div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="form-group col-12">
                                        <label class="font-weight-bold">@lang('Meta título')</label>
                                        <input type="text" class="form-control form-control-sm  @error('metaTitle') is-invalid @enderror" name="metaTitle" placeholder="Meta título" value="{{ old('metaTitle', $post->metaTitle) }}">
                                        @error('metaTitle')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                        @enderror
                                    </div>

                                    <div class="form-group col-12">
                                        <label class="font-weight-bold">@lang('Url')</label>
                                        <input type="text" class="form-control form-control-sm @error('slug') is-invalid @enderror" name="slug" placeholder="URL" value="{{ old('slug', $post->slug) }}">
                                        @error('slug')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                         @enderror
                                    </div>

                                    <div class="form-group col-12">
                                        <label class="font-weight-bold">@lang('Meta descripción')</label>
                                        <textarea class="form-control @error('metaDescription') is-invalid @enderror" name="metaDescription" placeholder="Meta descripción" rows="2">{{ old('metaDescription', $post->metaDescription) }}</textarea>
                                        @error('metaDescription')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">    
                        <div class="backend-card">
                            <div class="card-header">
                                <div class="backend-card-title">@lang('Miniatura')</div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="form-group col-12">
                                        <label class="font-weight-bold">@lang('Imagen actual')</label>
                                        <img class="img-thumbnail backend-image" src="{{ asset('storage/images/'.$post->postImage) }}">
                                    </div>
                                    <div class="form-group col-12">
                                        <input type="file" class="form-control-file @error('postImage') is-invalid @enderror" name="postImage">
                                    
                                        @error('postImage')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="backend-card">
                            <div class="card-header">
                                <div class="backend-card-title">@lang('Opciones')</div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="form-group col-12">                            
                                        <div class="custom-control custom-switch">
                                            <input type="hidden" name="visible" value="0">
                                            <input type="checkbox" class="custom-control-input @error('visible') is-invalid @enderror" name="visible" id="visible" value="1" @if(old('visible', $post->visible)) checked @endif>
                                            <label class="custom-control-label" for="visible">@lang('Visible')</label>
                                            @error('visible')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group col-12">                            
                                        <div class="custom-control custom-switch">
                                            <input type="hidden" name="favorite" value="0">
                                            <input type="checkbox" class="custom-control-input @error('favorite') is-invalid @enderror" name="favorite" id="favorite" value="1" @if(old('favorite', $post->favorite)) checked @endif>
                                            <label class="custom-control-label" for="favorite">@lang('Favorito')</label>
                                            @error('favorite')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection