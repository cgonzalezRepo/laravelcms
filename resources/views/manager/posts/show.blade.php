@extends('layouts.manager')
@section('title', 'Ver artículo')
@section('description', 'Ver un artículo desde el backend')

@section('content')
    <div class="row mt-3">
        <div class=" col-12 text-right">
            <a href="{{ url()->previous() }}" role="button" class="btn btn-success"><i class="fas fa-arrow-left"></i> Volver</a>
            @if($post->user->id == Auth::user()->id || Auth::user()->role_id == 1 || Auth::user()->role_id == 2)
                <a href="{{route('articulos.edit', $post->id)}}" role="button" class="btn btn-primary"><i class="far fa-edit"></i> Editar</a>
            @endif
        </div>
    </div>
    <div class="row flex-column-reverse flex-xl-row">
        <div class="col-xl-8 col-12">
            <div class="backend-card">
                <div class="card-header">
                    <div class="backend-card-title">Vista previa del artículo</div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <h1>{{$post->title}}</h1>
                        </div>

                        <div class="col-12">
                            <p>{!! $post->body !!}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-4 col-12">
            <div class="row">
                <div class="col-12">
                    <div class="backend-card">
                        <div class="card-header">
                            <div class="backend-card-title">Información general</div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <label class="font-weight-bold">Autor: </label><a href="{{route('usuarios.show', $post->user->id)}}"> {{$post->user->username}}</a>
                                </div>
                                <div class="col-12">
                                    <label class="font-weight-bold">Categorías: </label>
                                    <p class="d-inline"> 
                                        @if($post->categories->count() > 0)
                                            @foreach ($post->categories as $category)
                                                {{$loop->first ? '' : ', '}}
                                                <a href="{{route('categorias.show', ['id' => $category->id])}}">{{$category->name}}</a>     
                                            @endforeach
                                        @else
                                                Sin categoría
                                        @endif
                                    </p>
                                </div>

                                <div class="col-12">
                                    <p class="font-weight-bold d-inline">Visible: </p>
                                    @if($post->visible)
                                        <i class="fas fa-check text-success"></i>
                                    @else
                                        <i class="fas fa-times text-danger"></i>
                                    @endif
                                </div>

                                <div class="col-12 text-center">
                                    <label class="font-weight-bold d-block">Miniatura</label>
                                    <img class="img-thumbnail backend-image" src="{{ asset('storage/images/'.$post->postImage) }}">
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
            <div class="row">
                <div class="col-12">
                    <div class="backend-card">
                        <div class="card-header">
                            <div class="backend-card-title">Apartado SEO</div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <label class="font-weight-bold">Link: </label><a href="{{$post->slug}}"> {{route('home')}}/{{$post->slug}}</a>
                                </div>

                                <div class="col-12">
                                     <label class="font-weight-bold">Título de la página: </label><p class="d-inline"> {{$post->metaTitle}}</p>
                                </div>

                                <div class="col-12">
                                    <label class="font-weight-bold">Descripción de la página: </label><p class="d-inline"> {{$post->metaDescription}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection