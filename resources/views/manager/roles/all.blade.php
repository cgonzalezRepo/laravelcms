@extends('layouts.manager')
@section('title', 'Listado de roles')
@section('description', 'Listado de todos los roles')

@section('content')

	<div class="row">
		<div class="col-12">
			<div class="backend-card">
	            <div class="card-header">
	            	<h1><i class="far fa-folder-open"></i> Roles</h1>
	            </div>
	            <div class="card-body">
	                <div class="row">
	                    <div class="col-12">
							<table id="dataTable" class="table table-hover table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
								<thead class="thead-dark"> 
									<tr> 
										<th>ID</th>
										<th>Nombre</th>
										<th>Descripción</th>
										<th>Usuarios</th>
										<th>Acciones</th>
									</tr>
								</thead> 
								<tbody> 
									@foreach ($roles as $role)
										<tr> 
											<td>{{$role->id}}</td>
											<td>{{$role->name}}</td>
											<td>{{$role->description}}</td>
											<td>{{count($role->users)}}</td>
											<td>
												<div class="btn-group d-inline d-xl-none" role="group">
													<a href="{{route('roles.show', $role->id)}}" role="button" class="btn btn-success"><i class="far fa-eye"></i></a>
												</div>

												<div class="d-none d-xl-block">
													<a href="{{route('roles.show', $role->id)}}" role="button" class="btn btn-outline-success"><i class="far fa-eye"></i> Ver</a>
												</div>
											</td>
										</tr>
									@endforeach
								</tbody> 
							</table>
	                    </div>
	                </div>
	            </div>
	        </div>
    	</div>
	</div>
@endsection

