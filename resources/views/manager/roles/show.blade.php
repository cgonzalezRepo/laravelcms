@extends('layouts.manager')
@section('title', 'Ver rol')
@section('description', 'Ver un rol desde el backend')

@section('content')

    <div class="row mt-3">
        <div class=" col-12 text-right">
            <a href="{{ url()->previous() }}" role="button" class="btn btn-success"><i class="fas fa-arrow-left"></i> Volver</a>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="backend-card">
                <div class="card-header">
                    <div class="backend-card-title">Vista previa del rol</div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <label class="font-weight-bold">Nombre: </label><p class="d-inline"> {{$role->name}}</p>
                        </div>

                        <div class="col-12">
                             <label class="font-weight-bold">Descripción: </label><p class="d-inline"> {{$role->description}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12">
            <div class="backend-card">
                <div class="card-header">
                    <div class="backend-card-title">Usuarios que tienen este rol</div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <table id="dataTable" class="table table-hover table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                <thead class="thead-dark"> 
                                    <tr> 
                                        <th>ID</th>
                                        <th>Nombre de usuario</th>
                                        <th>Email</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead> 
                                <tbody> 
                                    @foreach ($role->users as $user)
                                        <tr> 
                                            <td>{{$user->id}}</td>
                                            <td><a href="{{route('usuarios.show', $user->id)}}">{{$user->username}}</a></td>
                                            <td>{{$user->email}}</td>
                                            <td>
                                                <div class="btn-group d-inline d-xl-none" role="group">
                                                    <a href="{{route('usuarios.show', $user->id)}}" role="button" class="btn btn-success"><i class="far fa-eye"></i></a>
                                                    <a href="{{route('usuarios.edit', $user->id)}}" role="button" class="btn btn-primary"><i class="far fa-edit"></i></a>
                                                </div>

                                                <div class="d-none d-xl-block">
                                                    <a href="{{route('usuarios.show', $user->id)}}" role="button" class="btn btn-outline-success"><i class="far fa-eye"></i> Ver</a>
                                                    <a href="{{route('usuarios.edit', $user->id)}}" role="button" class="btn btn-outline-primary"><i class="far fa-edit"></i> Editar</a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody> 
                            </table>    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection