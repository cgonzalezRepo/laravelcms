@extends('layouts.manager')
@section('title', 'Listado de redes sociales')
@section('description', 'Listado de redes sociales')
@section('content')
	@include('elements.modalConfirmDelete', ['id' => 'delete', 'title' => 'Eliminar red social', 'message' => 'Esta acción no se puede deshacer', 'route' => 'redes.destroy'])

	<div class="row">
		<div class="col-12">
			<div class="backend-card">
	            <div class="card-header">
					<h1><i class="far fa-folder-open"></i> @lang('Redes sociales')</h1>
	            </div>
	            <div class="card-body">
	                <div class="row">
	                    <div class="col-12">
							<a role="button" href="{{ route('redes.create') }}" class="btn btn-success pull-left"><i class="fas fa-plus"></i> @lang('Nuevo')</a>
	                    </div>
	                </div>
	                <div class="row">
	                    <div class="col-12">
							<table id="dataTable" class="table table-hover table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
								<thead class="thead-dark"> 
									<tr> 
										<th>Icono</th>
										<th class="datetable-title">Link</th>
										<th>Visible</th>
										<th>Creado</th> 
										<th>Actualizado</th>
										<th>Acciones</th>
									</tr>
								</thead> 
								<tbody> 
									@foreach ($socials as $social)
										<tr> 
											<td class="text-center"><i class="{{$social->icon}}"></i></td>
											<td><a rel="nofollow" href="{{$social->url}}">{{$social->url}}</a></td>
											<td class="text-center">
												<span class="d-none">{{$social->visible}}</span>
												@if($social->visible)
													<i class="fas fa-check text-success"></i>
												@else
													<i class="fas fa-times text-danger"></i>
												@endif
											</td>
											<td>{{$social->created_at}}</td>
											<td>{{$social->updated_at}}</td>
											<td>
												@include('elements.action-buttons', ['show' => 'redes.show', 'edit' => 'redes.edit', 'id' => $social->id])
											</td>
										</tr>
									@endforeach
								</tbody> 
							</table>
	                    </div>
	                </div>
	            </div>
	        </div>
    	</div>
	</div>
@endsection

