@extends('layouts.manager')
@section('title', 'Crear nuevo enlace a red social')
@section('description', 'Creación de nuevos enlaces a redes sociales desde el backend')

@section('content')
    <form class="form-horizontal col-12" action="{{ route('redes.store') }}" method="POST">
        @csrf
        <div class="row mt-3">
            <div class=" col-12 text-right">
                <a href="{{route('redes.index')}}" role="button" class="btn btn-success"><i class="fas fa-arrow-left"></i> @lang('Volver')</a>
                <button type="submit" class="btn btn-primary">@lang('Crear')</button>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="backend-card">
                    <div class="card-header">
                        <div class="backend-card-title">@lang('Crear nueva red social')</div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="alert alert-info" role="alert">
                                    @lang('Las redes sociales de esta lista se mostrarán en la sección "Redes sociales" de la página inicial.')
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-12">
                                <input type="text" class="form-control form-control-sm  @error('url') is-invalid @enderror" name="url" placeholder="Link" value="{{ old('url') }}">
                                @error('url')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>

                            <div class="form-group col-12">
                                <input type="text" class="form-control form-control-sm  @error('icon') is-invalid @enderror" name="icon" placeholder="Nombre del icono" value="{{ old('icon') }}">
                                @error('icon')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>

                            <div class="col-12">
                                <div class="alert alert-warning" role="alert">
                                    Este proyecto usa Font Awesome para mostrar iconos, puedes buscar <a rel="nofollow" href="https://fontawesome.com/icons?d=gallery">aquí</a> el que deseas y pegar su clase en el campo "Nombre del icono", ejemplo: facebook-f
                                </div>
                            </div>

                            <div class="form-group col-12">                            
                                <div class="custom-control custom-switch">
                                    <input type="hidden" name="visible" value="0">
                                    <input type="checkbox" class="custom-control-input @error('visible') is-invalid @enderror" name="visible" id="visible" value="1" @if(old('visible')) checked @endif>
                                    <label class="custom-control-label" for="visible">Visible</label>
                                    @error('visible')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection