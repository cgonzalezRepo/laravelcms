@extends('layouts.manager')
@section('title', 'Ver redes sociales')
@section('description', 'Ver una red social desde el backend')

@section('content')
{{-- @include('manager.categories.modalConfirmQuit') --}}

    <div class="row mt-3">
        <div class=" col-12 text-right">
            <a href="{{route('redes.index')}}" role="button" class="btn btn-success"><i class="fas fa-arrow-left"></i> Volver</a>
            <a href="{{route('redes.edit', $social->id)}}" role="button" class="btn btn-primary"><i class="far fa-edit"></i> Editar</a> 
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="backend-card">
                <div class="card-header">
                    <div class="backend-card-title">Vista previa de la red social</div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <label class="font-weight-bold">Link: </label><p class="d-inline"> {{$social->url}}</p>
                        </div>

                        <div class="col-12">
                             <label class="font-weight-bold">Nombre del icono: </label><p class="d-inline"> {{$social->icon}}</p>
                        </div>

                        <div class="col-12">
                             <label class="font-weight-bold">Icono: </label><p class="d-inline"> <i class="{{$social->icon}}"></i></p>
                        </div>

                        <div class="col-12">
                            <p class="font-weight-bold d-inline">Visible: </p>
                            @if($social->visible)
                                <i class="fas fa-check text-success"></i>
                            @else
                                <i class="fas fa-times text-danger"></i>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection