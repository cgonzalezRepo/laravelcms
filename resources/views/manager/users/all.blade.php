@extends('layouts.manager')
@section('title', 'Listado de usuarios')
@section('description', 'Listado de usuarios en el backend')
@section('content')
	@include('manager.users.modalConfirmDelete')

		<div class="row">
		<div class="col-12">
			<div class="backend-card">
	            <div class="card-header">
	            	<h1><i class="far fa-folder-open"></i> Usuarios</h1>
	            </div>
	            <div class="card-body">
	                <div class="row">
	                    <div class="col-12">
							<a role="button" href="{{ route('usuarios.create') }}" class="btn btn-success pull-left"><i class="fas fa-plus"></i> Nuevo</a>
	                    </div>
	                </div>
	                <div class="row">
	                    <div class="col-12">
							<table id="dataTable" class="table table-hover table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
								<thead class="thead-dark"> 
									<tr> 
										<th>ID</th> 
										<th>Nombre</th>
										<th>Rol</th>
										<th>Artículos</th>
										<th>Categorias</th>
										<th>Creado</th> 
										<th>Acciones</th>
									</tr>
								</thead> 
								<tbody> 
									@foreach ($users as $user)
										<tr> 
											<td>{{$user->id}}</td>
											<td>{{$user->username}}</td>
											<td>
												<a href="{{route('roles.show', ['id' => $user->role])}}">{{$user->role->name}}</a> 	
											</td>
											<td>{{count($user->posts)}}</td>
											<td>{{count($user->categories)}}</td>
											<td>{{$user->created_at}}</td>
											<td>
												<div class="btn-group d-inline d-xl-none" role="group">
													<a href="{{route('usuarios.show', $user->id)}}" role="button" class="btn btn-success"><i class="far fa-eye"></i></a>
												 	<a href="{{route('usuarios.edit', $user->id)}}" role="button" class="btn btn-primary"><i class="far fa-edit"></i></a>	
												 	<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal" data-id="{{$user->id}}"><i class="far fa-trash-alt"></i></button>
												</div>

												<div class="d-none d-xl-block">
													<a href="{{route('usuarios.show', $user->id)}}" role="button" class="btn btn-outline-success"><i class="far fa-eye"></i> Ver</a>
				      								<a href="{{route('usuarios.edit', $user->id)}}" role="button" class="btn btn-outline-primary"><i class="far fa-edit"></i> Editar</a>
													<button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#deleteModal" data-id="{{$user->id}}"><i class="far fa-trash-alt"></i> Eliminar</button>
												</div>
											</td>
										</tr>
									@endforeach
								</tbody> 
							</table>
	                    </div>
	                </div>
	            </div>
	        </div>
    	</div>
	</div>
@endsection

