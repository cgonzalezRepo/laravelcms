@extends('layouts.manager')
@section('title', 'Crear nuevo usuario')
@section('description', 'Creación de nuevos usuarios desde el backend')

@section('content')

    <form class="form-horizontal col-12" action="{{ route('usuarios.store') }}" method="POST">
        @csrf
        <div class="row mt-3">
            <div class=" col-12 text-right">
                <a href="{{route('usuarios.index')}}" role="button" class="btn btn-success"><i class="fas fa-arrow-left"></i> @lang('Volver')</a>
                <button type="submit" class="btn btn-primary">@lang('Crear')</button>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="backend-card">
                    <div class="card-header">
                        <div class="backend-card-title">@lang('Crear nuevo usuario')</div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-12">                            
                                <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="name" autofocus placeholder="@lang('Nombre de usuario') *">
                                @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                 @enderror
                            </div>
                            
                            <div class="form-group col-12 input-group" id="showHidePassword">
                                <input type="password" class="form-control @error('password') is-invalid @enderror" required autocomplete="new-password" name="password" placeholder="@lang('Contraseña') *">
                                <div class="input-group-append">
                                    <button type="button" class="btn btn-outline-secondary"><i class="fa fa-eye-slash" aria-hidden="true"></i></button>
                                </div>
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                 @enderror
                            </div>

                            <div class="form-group col-12 input-group" id="showHidePasswordRepeat">
                                <input id="password-confirm" type="password" class="form-control" placeholder="@lang('Repita contraseña') *" name="password_confirmation" required autocomplete="new-password">
                                <div class="input-group-append">
                                    <button type="button" class="btn btn-outline-secondary"><i class="fa fa-eye-slash" aria-hidden="true"></i></button>
                                </div>
                            </div>

                            <div class="form-group col-12">                            
                                <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="@lang('Email') *" required autocomplete="email" value="{{ old('email') }}">
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                 @enderror
                            </div>

                            <div class="form-group col-12">                            
                                <select class="form-control selectpicker" required name="role_id" title="Rol *">
                                    @foreach ($roles as $role)
                                        <option {{$role->id == old('role_id') ? "selected" : "" }} value="{{$role->id}}">{{$role->name}}</option>
                                    @endforeach
                                </select>
                                @error('role_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                 @enderror
                            </div>    
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="backend-card">
                    <div class="card-header">
                        <div class="backend-card-title">@lang('Información personal')</div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-12">                            
                                <input type="text" class="form-control @error('username') is-invalid @enderror" name="firstname" placeholder="@lang('Nombre')" value="{{ old('firstname') }}">
                                @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                 @enderror
                            </div>

                            <div class="form-group col-12">                            
                                <input type="text" class="form-control @error('lastname') is-invalid @enderror" name="lastname" placeholder="@lang('Apellidos')" value="{{ old('lastname') }}">
                                @error('lastname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                 @enderror
                            </div>

                            <div class="form-group col-12">                            
                                <input type="url" class="form-control @error('website') is-invalid @enderror" name="website" placeholder="@lang('Web')" value="{{ old('website') }}">
                                @error('website')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                 @enderror
                            </div>    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection