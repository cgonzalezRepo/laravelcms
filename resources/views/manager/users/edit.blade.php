@extends('layouts.manager')
@section('title', 'Editar usuario')
@section('description', 'Edición de un usuario desde el backend')

@section('content')
    <form class="form-horizontal col-12" action="{{ route('usuarios.update', $user->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="row mt-3">
            <div class="col-12 text-right">
                <a href="{{ url()->previous() }}" role="button" class="btn btn-success"><i class="fas fa-arrow-left"></i> Volver</a>
                <button type="submit" class="btn btn-primary">Actualizar</button>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-6 col-12">
                <div class="backend-card">
                    <div class="card-header">
                        <div class="backend-card-title">Editar usuario</div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-12">                            
                                <label class="font-weight-bold">Nombre de usuario</label>
                                <input type="text" class="form-control @error('username') is-invalid @enderror" name="username" placeholder="Nombre de usuario" value="{{ old('username', $user->username) }}">
                                @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                 @enderror
                            </div>
                            
                            <div class="form-group col-12" id="showHidePassword"> 
                                <label class="font-weight-bold">Contraseña</label>
                                <div class="input-group">
                                    <input type="password" class="form-control @error('password') is-invalid @enderror" autocomplete="new-password" name="password" placeholder="Contraseña *">
                                    <div class="input-group-append">
                                        <button type="button" class="btn btn-outline-secondary"><i class="fa fa-eye-slash" aria-hidden="true"></i></button>
                                    </div>
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                     @enderror
                                </div>
                            </div>
                                
                            <div class="form-group col-12" id="showHidePasswordRepeat">   
                                <label class="font-weight-bold">Confirmar contraseña</label>
                                <div class="input-group">
                                    <input id="password-confirm" type="password" class="form-control" placeholder="Repita contraseña *" name="password_confirmation" autocomplete="new-password">
                                    <div class="input-group-append">
                                        <button type="button" class="btn btn-outline-secondary"><i class="fa fa-eye-slash" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-12">
                                <label class="font-weight-bold">Email</label>
                                <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Email *" autocomplete="email" value="{{ old('email', $user->email) }}">
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                 @enderror
                            </div>

                            <div class="form-group col-12">
                                <label class="font-weight-bold">Rol</label>
                                <select class="form-control selectpicker" name="role_id" title="Rol">
                                    @foreach ($roles as $role)
                                        @if ($user->role->id == $role->id)
                                            <option selected value="{{$role->id}}">{{$role->name}}</option>
                                        @else
                                            <option value="{{$role->id}}">{{$role->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                                @error('role_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                 @enderror
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-6 col-12">
                <div class="backend-card">
                    <div class="card-header">
                        <div class="backend-card-title">Editar información personal</div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-12">
                                <label class="font-weight-bold">Nombre</label>
                                <input type="text" class="form-control @error('firstname') is-invalid @enderror" name="firstname" placeholder="Nombre" value="{{ old('firstname', $user->firstname) }}">
                                @error('firstname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                 @enderror
                            </div>

                            <div class="form-group col-12">
                                <label class="font-weight-bold">Apellidos</label>
                                <input type="text" class="form-control @error('lastname') is-invalid @enderror" name="lastname" placeholder="Apellidos" value="{{ old('lastname', $user->lastname) }}">
                                @error('lastname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                 @enderror
                            </div>

                            <div class="form-group col-12">
                                <label class="font-weight-bold">Dirección web</label>
                                <input type="url" class="form-control @error('website') is-invalid @enderror" name="website" placeholder="Web" value="{{ old('website', $user->website) }}">
                                @error('website')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                 @enderror
                            </div>
                        </div>
                    </div>
                </div>    
            </div>
        </div>
    </form>
@endsection