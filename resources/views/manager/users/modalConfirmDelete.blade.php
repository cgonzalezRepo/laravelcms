<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteUserModalConfirmaton" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="row">
          <div class="col-3">
            <i class="fas fa-exclamation-triangle fa-4x text-danger"></i>
          </div>
          <div class="col-9">
            <div class="row">
              <div class="col-12">
                 <h5 class="font-weight-bold">Eliminar usuario</h5>
              </div>
            </div>
            <div class="row">
              <div class="col-12">
                <p>Esta acción no se puede deshacer</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <form  id="deleteForm" action="{{route('usuarios.destroy', ['id' => 'placeHolderId'])}}" method="POST">
          @csrf
          @method('DELETE')
          <button type="button" class="btn" data-dismiss="modal">Cancelar</button>
          <button class="btn btn-danger" type="submit">Eliminar</button>
        </form>
      </div>
    </div>
  </div>
</div>