@extends('layouts.manager')
@section('title', 'Ver usuario')
@section('description', 'Ver un usuario desde el backend')

@section('content')

    <div class="row mt-3">
        <div class="col-12 text-right">
            <a href="{{ url()->previous() }}" role="button" class="btn btn-success"><i class="fas fa-arrow-left"></i> Volver</a>
            <a href="{{route('usuarios.edit', $user->id)}}" role="button" class="btn btn-primary"><i class="far fa-edit"></i> Editar</a> 
        </div>
    </div>

    <div class="row">
        <div class="col-12 col-xl-6">
            <div class="backend-card">
                <div class="card-header">
                    <div class="backend-card-title">Información del usuario</div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <label class="font-weight-bold">Nombre de usuario: </label><p class="d-inline"> {{$user->username}}</p>
                        </div>
                 
                        <div class="col-12">
                            <label class="font-weight-bold">Rol: </label><p class="d-inline"> <a href="{{route('roles.show', ['id' => $user->role])}}">{{$user->role->name}}</a></p>
                        </div>
                
                        <div class="col-12">
                            <label class="font-weight-bold">Creado en: </label><p class="d-inline"> {{$user->created_at}}</p>
                        </div>

                        <div class="col-12">
                             <label class="font-weight-bold">Email: </label><p class="d-inline"> {{$user->email}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-xl-6">
            <div class="backend-card">
                <div class="card-header">
                    <div class="backend-card-title">Información personal</div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <label class="font-weight-bold">Nombre: </label><p class="d-inline"> {{$user->firstname}}</p>
                        </div>

                        <div class="col-12">
                            <label class="font-weight-bold">Apellido: </label><p class="d-inline"> {{$user->lastname}}</p>
                        </div>
         
                        <div class="col-12">
                            <label class="font-weight-bold">Web: </label><p class="d-inline"> {{$user->website}}</p>
                        </div>
                    </div>
                </div>
            </div>    
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="backend-card">
                <div class="card-header">
                    <div class="backend-card-title">Contribución</div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 col-xl-6">
                            <div class="row">
                                <div class="col-12">
                                    <label class="font-weight-bold">Artículos creados</label>
                                </div>
                            </div> 
                            <div class="row">
                                <div class="col-12">
                                    {!! $chartPost->container() !!}
                                    {!! $chartPost->script() !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-xl-6 mt-4 mt-xl-0">
                            <div class="row">
                                <div class="col-12">
                                    <label class="font-weight-bold">Categorías creadas</label>
                                </div>
                            </div> 
                            <div class="row">
                                <div class="col-12">
                                    {!! $chartCategory->container() !!}
                                    {!! $chartCategory->script() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection