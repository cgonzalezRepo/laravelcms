@extends('layouts.default')
@section('title', 'Home title')
@section('description', 'This is the child meta description')

@section('topContent')
	@include('elements.breadcrumbs', ['actualPageName' => $user->username])
@endsection
@section('content')	
	<div class="row">
		<div class="col-12">
			<div class="separator"><h1>@lang('Autor'): {{$user->username}}</h1></div>
		</div>
	</div>
	<div class="row">
		@include('elements.posts', ['posts' => $userPosts, 'show' => 'col-sm-6', 'images' => 'card-custom-img-slide-2', 'body' => true, 'comments' => true, 'separator' => true])
	</div>
	<div class="row mt-4">
		<div class="col-12">
			{{ $userPosts->links() }}
		</div>
	</div>
@endsection