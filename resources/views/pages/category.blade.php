@extends('layouts.default')
@section('title', $category->metaTitle)
@section('description', $category->metaDescription)

@section('topContent')
	@include('elements.breadcrumbs', ['actualPageName' => $category->name])
@endsection
@section('content')	
	<div class="row">
		<div class="col-12">
			<div class="separator"><h1>@lang('Categoría'): {{$category->name}}</h1></div>
		</div>
	</div>
	<div class="row">
		@include('elements.posts', ['posts' => $categoryPosts, 'show' => 'col-sm-6', 'images' => 'card-custom-img-slide-2', 'body' => true, 'comments' => true, 'separator' => true])
	</div>
	<div class="row mt-4">
		<div class="col-12">
			{{ $categoryPosts->links() }}
		</div>
	</div>
@endsection