@extends('layouts.default')
@section('title', 'Home title')
@section('description', 'This is the child meta description')

@section('topContent')
	@if($favoritePosts->count() > 0)<!-- AUTHORS PICK -->
		<div class="row">
			<div class="col-8 col-md-10">
				<div class="separator"><h2>@lang('Favoritos del autor')</h2></div>
			</div>
			<div class="col-4 col-md-2 text-right align-text-top">
				<span class="owl-custom-nav" id="customNav"></span>
			</div>
		</div>
		<div class="row mb-5">
			<div class="owl-carousel owl-theme" id="owl-carousel-home">
				@include('elements.posts', ['posts' => $favoritePosts, 'show' => '', 'images' => 'card-custom-img-slide-4', 'body' => false, 'comments' => false, 'separator' => false])
			</div>
		</div>
	@endif<!-- END AUTHORS PICK -->
@endsection
@section('content')
	<!-- LATEST POST -->
	<div class="row">
		<div class="col-12">
			<div class="separator"><h2>@lang('Últimos artículos')</h2></div>
		</div>			
	</div>
	@foreach($posts as $post)
		<div class="row mb-4">
			<a class="col-12 col-md-6 custom-card" href="{{route('post', ['id' => $post->slug])}}">
				<div class="wrap">
					<img class="card-custom-img-post" src="{{ asset('storage/images/'.$post->postImage) }}" alt="alt image">
					<div class="middle">
						<button type="button" class="btn btn-lg btn-outline-light">@lang('Leer más')</button>
					</div>
				</div>
			</a>
			<div class="col-12 col-md-6 mt-3 mt-md-0">
				<div class="pull-left"> 
					@foreach ($post->categories as $category)
						<span class="card-custom-title">{{$loop->first ? '' : ', '}}</span>
						<a href="{{route('category', ['id' => $category->slug])}}" class="card-custom-title no-decoration-link">{{$category->name}}</a>	
					@endforeach
				</div>
				<a href="{{route('post', ['id' => $post->slug])}}" class="no-decoration-link subtitle-post-link gray-hover">
					<h2 class="card-custom-subtitle gray-hover-post">{{$post->title}}</h2>
				</a>
				<div class="card-custom-text-post">{{ str_limit(strip_tags($post->body), 200) }}</div>
				<p class="card-custom-text mt-auto">
					<p><a href="{{route('autor', ['id' => $post->user->username])}}" class="no-decoration-link gray-hover">{{$post->user->username}}</a>, <i class="far fa-clock"></i> {{ $post->created_at->diffForHumans() }} <a class="no-decoration-link gray-hover" href="{{route('post', ['id' => $post->slug]).'#comments'}}"><i class="far fa-comment ml-4"></i> {{$post->comments->count()}}</a></p>
				</p>
			</div>
		</div>
	@endforeach
	
	<div class="row mb-5 mb-lg-0">
		<div class="col-12">
			{{ $posts->links() }}
		</div>
	</div>
	<!-- END LATEST POST -->
@endsection