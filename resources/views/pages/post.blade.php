@extends('layouts.default')
@section('title', $post->metaTitle)
@section('description', $post->metaDescription)

@section('topContent')
	@include('elements.breadcrumbs', ['actualPageName' => $post->title])
@endsection
@section('content')
	<div class="row">
		<div class="col-12">
			<img class="post-image" src="{{ asset('storage/images/'.$post->postImage) }}">
		</div>
		<div class="col-12">
			<p class="mt-3"> 
				@foreach ($post->categories as $category)
					<span class="card-custom-title">{{$loop->first ? '' : ', '}}</span>
					<a href="{{route('category', ['id' => $category->id])}}" class="card-custom-title no-decoration-link">{{$category->name}}</a>	
				@endforeach
	        </p>
		</div>
		<div class="col-12">
			<h1 class="title-post">{{$post->title}}</h1>
			<p class="card-custom-text"><i class="far fa-clock"></i> <a href="{{route('autor', ['id' => $post->user->id])}}" class="no-decoration-link gray-hover">{{$post->user->username}}</a>, {{ $post->created_at->diffForHumans() }}</p>
		</div>

		<div class="col-12 mb-5">
			<p>{!! $post->body !!}</p>
		</div>

		<div class="col-12 mt-5"><!-- Carousel articulos relacionados -->			
			<div class="row">
				<div class="col-8 col-md-10">
					<div class="separator"><h3 class="custom-h2">@lang('Artículos relacionados')</h3></div>
				</div>
				<div class="col-4 col-md-2 text-right align-text-top">
					<span class="owl-custom-nav" id="customNav"></span>
				</div>
			</div>

			<div class="row">
				<div class="owl-carousel owl-theme" id="owl-carousel-post">
					@include('elements.posts', ['posts' => $relatedPosts, 'show' => '', 'images' => 'card-custom-img-slide', 'body' => false, 'comments' => false, 'separator' => false])
				</div>
			</div>
		</div><!-- END Carousel articulos relacionados -->
	</div>
	<div class="row mt-5" id="comments">
		<div class="col-12">
			<div class="separator"><h3 class="custom-h2">{{$post->comments->count()}} @choice('messages.comentario', $post->comments->count())</h3></div>
		</div>
		@foreach($post->comments as $comment)
			<div class="col-12 {{$comment->author ? "alert alert-secondary" : "" }}">
				<div class="row">	
					<div class="col-3 col-md-2 text-center">
						<img src="{{$comment->author ? asset('images/logo.png') : asset('images/default_avatar.jpg') }}" class="img-round" alt="User avatar">
					</div>
					<div class="col-9 col-md-10">
						<div class="row">
							<div class="col-12">
								<p class="custom-h3 mb-1">{{$comment->name}}</p>
								<p class="mb-0">{{$comment->body}}</p>
								<p class="card-custom-text"><i class="far fa-clock"></i> {{ $comment->created_at->diffForHumans() }}</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			@if($post->comments->count() > 1 && !$loop->last)
				<div class="col-12">
					<hr>
				</div>
			@endif
		@endforeach
	</div>
	<div class="row comment-box my-5 mx-1 mx-md-1" id="commentBox">
		<div class="col-12 mb-3">
			<h4 class="custom-h3">@lang('Dejar un comentario')</h4>
		</div>
		<div class="col-12">
			<form action="{{ route('comentarios.store') }}" method="POST">
				@csrf
				<div class="form-row">
					<div class="form-group col-12">
						<input type="hidden" name="post_id" id="post_id" value="{{$post->id}}">
						<label for="inputComment4" class="card-custom-text font-weight-bold">@lang('Comentario') *</label>
                        <textarea class="form-control @error('body') is-invalid @enderror" name="body" rows="5">{{ old('body') }}</textarea>
                        @error('body')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                         @enderror
                    </div>    
			  	</div>
			  	<div class="form-row">
					<div class="form-group col-12 col-md-4">
						<label for="inputName4" class="card-custom-text font-weight-bold">@lang('Nombre') *</label>
						<input type="text" {{ old('name', Auth::check() ? 'readonly' : '') }} class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name', Auth::check() ? Auth::user()->username : '') }}">
						@error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                         @enderror
					</div>
					<div class="form-group col-12 col-md-4">
						<label for="inputEmail4" class="card-custom-text font-weight-bold">@lang('Email')</label>
						<input type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}">
						@error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                         @enderror
					</div>
					<div class="form-group col-12 col-md-4">
						<label for="inputWebl4" class="card-custom-text font-weight-bold">@lang('Web')</label>
						<input type="text" class="form-control @error('website') is-invalid @enderror" name="website" value="{{ old('website') }}">
						@error('website')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                         @enderror
					</div>
			  	</div>
			  	<div class="form-row mt-3 flex-column-reverse flex-md-row">
			  		<div class="form-group col-12 col-md-4">
			  			<button type="submit" class="btn btn-dark">@lang('Comentar')</button>
			  		</div>
			  		<div class="form-group col-12 col-md-8 text-md-right">
			  			<p>@lang('Los campos obligatorios están marcados') *</p>
			  		</div>
			  	</div>
			</form>
		</div>
	</div>
@endsection