@extends('layouts.default')
@section('title', 'Resultados de la búsqueda')
@section('description', 'Buscador')

@section('topContent')
	@include('elements.breadcrumbs', ['actualPageName' => "Resultados de búsqueda"])
@endsection
@section('content')	
	<div class="row">
		<div class="col-12">
			<div class="separator"><h1>@lang('Resultados de búsqueda'): {{$inputSearch}}</h1></div>
		</div>
	</div>
	<div class="row">
		@if(count($posts) > 0)
			@include('elements.posts', ['posts' => $posts, 'show' => 'col-sm-6', 'images' => 'card-custom-img-slide-2', 'body' => true, 'comments' => true, 'separator' => true])
		@else
			<div class="col-12">
				<p>@lang('No se han encontrado resultados')</p>
			</div>
		@endif
		
	</div>
	@if(count($posts) > 0)
		<div class="row mt-4">
			<div class="col-12">
				{{ $posts->links() }}
			</div>
		</div>
	@endif
@endsection