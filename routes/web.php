<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/manager', function () {
    return redirect()->route('login');
});

Route::resource('/comentarios', 'CommentController')->only([
    'store'
]);

Route::group(['prefix' => 'manager', 'middleware' => 'auth'], function()
{
	Route::get('/articulos/favoritos', 'PostController@showFavorites')->name('articulos.showFavorites');

	Route::resources([
	    '/articulos' => 'PostController',
	    '/categorias' => 'CategoryController',
	    '/usuarios' => 'UserController',
	    '/redes' => 'SocialController'
	]);

	Route::post('/articulos/{articulo}/quitar', 'PostController@quit')->name('articulos.quit');

	Route::post('/categorias/{articulo}/{categoria}/quitar', 'CategoryController@quit')->name('categorias.quit');

	Route::resource('/roles', 'RoleController')->only([
	    'index', 'show'
	]);

	Route::get('/configuracion/cabecera', 'ConfigController@header')->name('configuracion.header');
});

Route::group(['middleware' => 'shareData'], function(){
	Route::get('/', 'HomeController@index')->name('home');
	Route::get('/categoria/{slug}', 'HomeController@showPostByCategory')->name('category');
	Route::get('/autor/{username}', 'HomeController@showPostByUser')->name('autor');
	Route::get('/buscar', 'HomeController@search')->name('articulos.search');
	Route::get('/{slug}', 'HomeController@showPostBySlug')->name('post');
});